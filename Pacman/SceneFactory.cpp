/** Fonctions de cr�ation de la sc�ne */

#include "Scene.h"

void Scene::loadFromXML(const std::string filename)
{
	TiXmlDocument mapFile(filename.c_str());
	mapFile.LoadFile();

	TiXmlHandle hDoc(&mapFile);
	TiXmlElement* pElem = hDoc.FirstChildElement().Element();
	TiXmlHandle hRoot = TiXmlHandle(pElem);

	pElem = hRoot.FirstChild("texture").FirstChild().Element();
	VectPtrTexture texPacman;
	VectPtrTexture texGhost;
	VectPtrTexture texWall;
	VectPtrTexture texGum;

	loadTexture(pElem, texPacman, texGhost, texWall, texGum);

	pElem = hRoot.FirstChild("path").FirstChild().Element();
	initWallAndGum(pElem, texWall, texGum);

	initPacman(pElem, texPacman);
	initGhost(pElem, texGhost);

}

void Scene::loadTexture(TiXmlElement* pXmlTex, VectPtrTexture& texPac, VectPtrTexture& texGh, VectPtrTexture& texWall, VectPtrTexture& texGum)
{
	while (pXmlTex)
	{
		std::string sName = std::string(pXmlTex->Value());
		std::string sPath = std::string(pXmlTex->Attribute("path"));

		std::shared_ptr<sf::Texture> newTex = std::make_shared<sf::Texture>();

		if (newTex->loadFromFile(sPath) == false)
		{
			std::cout << "Unable to load texture " << sPath << std::endl;
			// Charger une texture par d�faut ???
		}
		else {
			_texVec.push_back(newTex);
			if (sName.find("Pacman") != std::string::npos)
				texPac.push_back(newTex);
			else if (sName.find("Ghost") != std::string::npos)
				texGh.push_back(newTex);
			else if (sName.find("Wall") != std::string::npos)
				texWall.push_back(newTex);
			else
				texGum.push_back(newTex);
		}

		pXmlTex = pXmlTex->NextSiblingElement();
	}

}

void Scene::initWallAndGum(TiXmlElement* pXmlWall, const VectPtrTexture& texWall, const VectPtrTexture& texGum)
{
	int xStart, yStart, xEnd, yEnd;
	VectPtrTexture texH, texV;
	texH.push_back(texWall[0]);
	texV.push_back(texWall[1]);
	while (pXmlWall)
	{
		pXmlWall->QueryIntAttribute("sx", &xStart);
		pXmlWall->QueryIntAttribute("sy", &yStart);
		pXmlWall->QueryIntAttribute("ex", &xEnd);
		pXmlWall->QueryIntAttribute("ey", &yEnd);

		if (xStart == xEnd) {
			for (int j = yStart; j <= yEnd; j++) {
				_vWall.push_back(std::make_shared<Wall>(GRID_SIZE*(xStart-1), GRID_SIZE*j, 20, false, texV));
				_vPacgum.push_back(std::make_shared<Pacgum>(GRID_SIZE*xStart, GRID_SIZE*j, 10, false, texGum));
				_vWall.push_back(std::make_shared<Wall>(GRID_SIZE*(xStart + 1), GRID_SIZE*j, 20, false, texV));
			}
		}
		else if (yStart == yEnd) {
			for (int i = xStart; i <= xEnd; i++) {
				_vWall.push_back(std::make_shared<Wall>(GRID_SIZE*i, GRID_SIZE*(yStart-1), 20, false, texH));
				_vPacgum.push_back(std::make_shared<Pacgum>(GRID_SIZE*i, GRID_SIZE*yStart, 10, false, texGum, CHERRY));
				_vWall.push_back(std::make_shared<Wall>(GRID_SIZE*i, GRID_SIZE*(yStart+1), 20, false, texH));
			}
		}
		else {
			std::cout << "Errror: a corridor must have two coordinates equal (x/x or y/y)" << std::endl;
		}
		
		pXmlWall = pXmlWall->NextSiblingElement();

	}
}

void Scene::initPacman(TiXmlElement* pXmlPac, const VectPtrTexture& texPac)
{
	_pacman = std::make_shared<Pacman>(20, 20, 20, false, texPac, LEFT, 0, 1);
}

void Scene::initGhost(TiXmlElement* pXmlGh, const VectPtrTexture& texGh)
{
	_vGhosts.push_back(std::make_shared<Ghost>(200, 200, 20, false, texGh, RIGHT, 0, 1, _pacman));
	_vGhosts[0]->setMainStrategy(std::shared_ptr<Strategy>(new Random), _pacman);
}