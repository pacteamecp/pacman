#include "GameEvent.h"
#include <algorithm> // find_if()

std::vector<std::shared_ptr<GameEvent>> GameEvent::eventList = std::vector<std::shared_ptr<GameEvent>>();

GameEvent::GameEvent()
{}

GameEvent::~GameEvent()
{}

int GameEvent::getID()
{
	return myID;
}

// Cr�ation d'un �v�nement avec l'ID donn�
// Si l'ID existe, la fonction retourne faux
bool GameEvent::create(int id)
{
	// V�rification que l'ID n'existe pas d�j�
	/*for (auto& e : eventList)
	{
		if (e->myID == id)
			return false;
	}*/
	myID = id;

	eventList.push_back(shared_from_this());

	return true;
}

void GameEvent::trigger(int arg)
{
	if (arg == 0)
		arg = myID;

	for (auto& l : myListenerList)
	{
		// Ex�cution de la fonction demand�e
		l->update(arg);
	}
}

bool GameEvent::listener_register(int id_event, std::shared_ptr<GameEventListener> listener)
{
	bool found = false;

	// Enregistrement aupr�s de tous les �v�nements
	for (auto& ev : eventList)
	{
		if (ev->myID == id_event) {
			ev->myListenerList.push_back(listener);
			found = true;
		}
	}
	
	return found;
}

void GameEvent::event_remove(int id_event)
{
	std::vector<std::shared_ptr<GameEvent>>::iterator it;
	it = eventList.begin();

	// Recherche de l'�v�nement donn�
	while (it < eventList.end())
	{
		if ((*it)->myID == id_event) {
			// Suppresion de l'�v�nement de la liste des �v�nements
			eventList.erase(it);
		}
		it++;
	}
}

void GameEvent::clear()
{
	for (auto& ev : eventList) {
		ev->myListenerList.clear();
	}
	eventList.clear();
}