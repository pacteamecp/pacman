#include "..\inc\Strategy.h"
#include "Math.h"
#include <random>


Strategy::~Strategy()
{
}

Direction Strategy::play()
{
	return apply();
}

void Strategy::setPacmanPtr(std::shared_ptr<Pacman> pacman)
{
	_pacman = pacman;
}

void Strategy::setGhostPtr(std::weak_ptr<Ghost> ghost)
{
	_ghost = ghost;
}


std::shared_ptr<Pacman> Strategy::getStrategyTarget()
{
	return _pacman;
}

std::weak_ptr<Ghost> Strategy::getStrategyGhost()
{
	return _ghost;
}

Random::Random()
{
	_impossibleDirections = std::vector<bool>(5, false);
	_dir = NONE;
	cptRand = 60;
}

Afraid::Afraid()
{
	_impossibleDirections = std::vector<bool>(5, false);
	_dir = NONE;
}


Track::Track()
{
	_impossibleDirections = std::vector<bool>(5, false);
	_dir = NONE;
}

Offensive::Offensive()
{
	_impossibleDirections = std::vector<bool>(5, false);
	_dir = NONE;
	cptRand = 60;
}

void Strategy::checkDir(Direction currentDir)
{
	if (currentDir != NONE)
	{
		for (int j = 1; j <= 4; j++)
		{
			_impossibleDirections[j] = false;
		}
	}
	if (currentDir != _dir)
	{
		_impossibleDirections[_dir] = true;
	}
}

void Strategy::chooseDefaultDir(Direction currentDir)
{
	if (currentDir == NONE)
	{
		int randNumber = (rand() % 4) + 5;
		int compteur = 0;
		while (compteur <= randNumber)
		{
			if (_impossibleDirections[(compteur % 4) + 1] == false)
			{
				_dir = Direction((compteur % 4) + 1);
			}
			compteur++;
		}
	}
}

Direction Offensive::apply()
{
	auto ghostPtr = _ghost.lock();
	sf::Vector2f ghostCoord = ghostPtr->getCoord();
	float ghostx = ghostCoord.x;
	float ghosty = ghostCoord.y;
	Direction currentDir = ghostPtr->getDir();
	checkDir(currentDir);
	sf::Vector2f pacCoord = _pacman->getCoord();
	float pacmanx = pacCoord.x;
	float pacmany = pacCoord.y;
	
	if ((abs(pacmanx - ghostx) + abs(pacmany - ghosty)) > 200)
	{
		if (cptRand > 0)
		{
			cptRand--;
		}
		else
		{
			if ((cptRand--) == 0)
			{
				cptRand = 60;
				_dir = Direction((rand() % 4) + 1);
			}
		}
	}
	else
	{
		if (abs(pacmanx - ghostx) > abs(pacmany - ghosty))
		{
			if (pacmanx > ghostx)
			{
				_dir = RIGHT;
			}
			else
			{
				_dir = LEFT;
			}
		}
		else
		{
			if (pacmany > ghosty)
			{
				_dir = DOWN;
			}
			else
			{
				_dir = UP;
			}
		}
	}
	chooseDefaultDir(currentDir);
	return _dir;
}

Direction Random::apply()
{
	auto ghostPtr = _ghost.lock();
	sf::Vector2f ghostCoord = ghostPtr->getCoord();
	float ghostx = ghostCoord.x;
	float ghosty = ghostCoord.y;
	Direction currentDir = ghostPtr->getDir();
	checkDir(currentDir);
	if (cptRand > 0)
	{
		cptRand--;
	}
	else 
	{
		if ((cptRand--) == 0)
		{
			cptRand = 60;
			//std::default_random_engine generator;
			_dir = Direction( (rand() % 4) + 1);
		}
	}
	chooseDefaultDir(currentDir);
	return _dir;
}

Direction Afraid::apply()
{
	auto ghostPtr = _ghost.lock();
	sf::Vector2f ghostCoord = ghostPtr->getCoord();
	float ghostx = ghostCoord.x;
	float ghosty = ghostCoord.y;
	Direction currentDir = ghostPtr->getDir();
	checkDir(currentDir);
	sf::Vector2f pacCoord = _pacman->getCoord();
	float pacmanx = pacCoord.x;
	float pacmany = pacCoord.y;
	if (abs(pacmanx - ghostx) < abs(pacmany - ghosty))
	{
		if (pacmanx > ghostx)
		{
			_dir = LEFT;
		}
		else
		{
			_dir = RIGHT;
		}
	}
	else
	{
		if (pacmany > ghosty)
		{
			_dir = UP;
		}
		else
		{
			_dir = DOWN;
		}
	}
	chooseDefaultDir(currentDir);
	return _dir;
	
}

Direction Track::apply()
{
	auto ghostPtr = _ghost.lock();
	sf::Vector2f ghostCoord = ghostPtr->getCoord();
	float ghostx = ghostCoord.x;
	float ghosty = ghostCoord.y;
	Direction currentDir = ghostPtr->getDir();
	checkDir(currentDir);
	sf::Vector2f pacCoord = _pacman->getCoord();
	float pacmanx = pacCoord.x;
	float pacmany = pacCoord.y;
	if (abs(pacmanx - ghostx) > abs(pacmany - ghosty))
	{
		if (pacmanx > ghostx)
		{
			_dir = RIGHT;
		}
		else
		{
			_dir = LEFT;
		}
	}
	else
	{
		if (pacmany > ghosty)
		{
			_dir = DOWN;
		}
		else
		{
			_dir = UP;
		}
	}
	chooseDefaultDir(currentDir);
	return _dir;
}