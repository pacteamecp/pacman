#include <iostream>
#include <windows.h>
#include "..\inc\Menu.h"

using namespace sf;

Menu::Menu()
{
}

Menu::Menu(MenuType type, PointerMouse& point)
{
	if (type == MENU_START){
		//chargement des textures
		listTexButton.push_back(std::make_shared<sf::Texture>());
		listTexButton.push_back(std::make_shared<sf::Texture>());
		if (!listTexButton[0]->loadFromFile("../img/start.png"))
			std::cout << "Impossible de charger la texture start.png" << std::endl;

		if (!listTexButton[1]->loadFromFile("../img/start2.png"))
			std::cout << "Impossible de charger la texture start2.png" << std::endl;

		listButton.push_back(Buttons(POS_BUTTON_ALL_X, POS_BUTTON_START_Y, BUTTON_X,
				VectPtrTexture(listTexButton.begin(), listTexButton.begin()+2), 1));

		listTexButton.push_back(std::make_shared<sf::Texture>());
		listTexButton.push_back(std::make_shared<sf::Texture>());
		if (!listTexButton[2]->loadFromFile("../img/quit.png"))
			std::cout << "Impossible de charger la texture quit.png" << std::endl;

		if (!listTexButton[3]->loadFromFile("../img/quit2.png"))
			std::cout << "Impossible de charger la texture quit2.png" << std::endl;

		listButton.push_back(Buttons(POS_BUTTON_ALL_X, POS_BUTTON_QUIT_Y, BUTTON_X,
			VectPtrTexture(listTexButton.begin()+2, listTexButton.begin() + 4), 4));
	}
	else if (type == MENU_PAUSE)
	{
		listTexButton.push_back(std::make_shared<sf::Texture>());
		listTexButton.push_back(std::make_shared<sf::Texture>());
		if (!listTexButton[0]->loadFromFile("../img/restart.png"))
			std::cout << "Impossible de charger la texture quit.png" << std::endl;

		if (!listTexButton[1]->loadFromFile("../img/restart2.png"))
			std::cout << "Impossible de charger la texture quit2.png" << std::endl;

		listButton.push_back(Buttons(POS_BUTTON_LARGE_X, POS_BUTTON_RESTART_Y, BUTTON_LARGE_X,
			VectPtrTexture(listTexButton.begin() , listTexButton.begin() + 2), 2));

		listTexButton.push_back(std::make_shared<sf::Texture>());
		listTexButton.push_back(std::make_shared<sf::Texture>());
		if (!listTexButton[2]->loadFromFile("../img/resume.png"))
			std::cout << "Impossible de charger la texture start.png" << std::endl;

		if (!listTexButton[3]->loadFromFile("../img/resume2.png"))
			std::cout << "Impossible de charger la texture start2.png" << std::endl;

		listButton.push_back(Buttons(POS_BUTTON_LARGE_X, POS_BUTTON_RESUME_Y, BUTTON_X,
			VectPtrTexture(listTexButton.begin() + 2, listTexButton.begin() + 4), 3));

		listTexButton.push_back(std::make_shared<sf::Texture>());
		listTexButton.push_back(std::make_shared<sf::Texture>());
		if (!listTexButton[4]->loadFromFile("../img/quit.png"))
			std::cout << "Impossible de charger la texture start.png" << std::endl;

		if (!listTexButton[5]->loadFromFile("../img/quit2.png"))
			std::cout << "Impossible de charger la texture start2.png" << std::endl;

		listButton.push_back(Buttons(POS_BUTTON_ALL_X, POS_BUTTON_QUIT_Y, BUTTON_LARGE_X,
			VectPtrTexture(listTexButton.begin() + 4, listTexButton.begin() + 6), 4));
	}

	listTexButton.push_back(std::make_shared<sf::Texture>());
	if (!listTexButton.back()->loadFromFile("../img/pacman_logo.png"))
		std::cout << "Impossible de charger la texture pacman_logo.png" << std::endl;
	pacmanLogo.setTexture(*(listTexButton.back()));
	pacmanLogo.setPosition(320, 350);
}

Menu::~Menu()
{
}

int Menu::show(sf::RenderWindow &window, sf::Event event)
{
	Mouse mouse;
	ret = 0;
	while (!ret)
	{
		window.clear();
		for (auto& b : listButton){
			b.draw(window);
		}
		window.draw(pacmanLogo);
		window.display();

		//recuperation de la position de la souris
		sf::Vector2i mouseCoord = mouse.getPosition(window);
		point.setPosition(mouseCoord);
		for (auto& b : listButton) {
			//teste de collision
			if (point.Collide(b.getCoord(), b.getHitbox())){
				b.isFocus();
				//test de click
				if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left)
				{
						ret = b.getID();
				}

			}
			else {
				if (b.is_focused)
				{
					b.deFocus();
				}
			}
		}

	

		window.pollEvent(event);
		if (event.type == sf::Event::Closed)
			ret = BUTTON_QUIT;
	}
	return ret;
}