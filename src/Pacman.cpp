#include <iostream>
#include <math.h>
#include "Pacman.h"
#include "Pacgum.h"

Pacman::Pacman(float x0, float y0, float hb, VectPtrTexture textVect, Direction dir, int hp, float speed)
	: MovingObject(x0, y0, hb, textVect, dir, hp, speed)
{
	_killEvent->create(ID_PACMAN_KILLED);

	_deadEvent = std::make_shared<GameEvent>();
	_deadEvent->create(ID_PACMAN_DEAD);
}

void Pacman::update_spriteNumber()
{
	// Changer le sprite seulement si le Pacman bouge, sinon on garde la précédente
	if(_dir != NONE)
		_spriteNumber = _dir;
}

void Pacman::draw(sf::RenderWindow &win)
{
	static int cptOpen = 2*T_OPEN_CLOSE;
	if (_state == ALIVE)
	{
		if (cptOpen > T_OPEN_CLOSE){
			win.draw(_spriteVect[_spriteNumber]);
			cptOpen--;
		}
		else {
			_spriteVect[SPRITE_CLOSE].setPosition(_spriteVect[_spriteNumber].getPosition());
			win.draw(_spriteVect[SPRITE_CLOSE]);
			if ((cptOpen--) == 0)
				cptOpen = 2*T_OPEN_CLOSE;
		}
	}
}

bool Pacman::updateSpecialEffectTimer()
{
	if (_specialEffectTimer > 0)
	{ 
		_specialEffectTimer--;
	}
	else
	{
		setSpeed(_baseSpeed);
	}
	return (_specialEffectTimer <= 0);
}

void Pacman::kill()
{
	_hp = _hp - 1;
	if (_hp > 0)
	{
		_state = ALIVE;
		_killEvent->trigger();
		respawn();
	}
	else {
		_state = DEAD;
		_deadEvent->trigger();
	}
	_specialEffectTimer = 0;
	updateSpecialEffectTimer();
}

void Pacman::update(int arg)
{
	switch (arg)
	{
	case ID_PEPPER_EATEN:
		//std::cout << "PEPPER : BOOST!!!" << std::endl;
		setSpeed(std::min(2*_baseSpeed, SPEED_MAX));
		setSpecialEffectTimer(PACMAN_PEPPER_TIMEOUT);
		break;

	default:
		std::cout << "Error : unhandled event : " << arg << std::endl;
		break;
	}
	
}
