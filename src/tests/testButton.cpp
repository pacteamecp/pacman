#include "tests.h"
#include "Buttons.h"
#include"Menu.h"
#include <iostream>

using namespace sf;


void test_button(sf::RenderWindow &window, sf::Event &event)
{
	int retValue = 0;

	PointerMouse point;
	Menu menu(MENU_PAUSE, point);


	while (window.isOpen() && !retValue)
	{
		while (window.pollEvent(event))
		{
			// If the red cross is pressed, close the window (and exit the game)
			if (event.type == sf::Event::Closed)
				window.close();
			else
				retValue = menu.show(window,event);
				//affichage de la valeur
				std::cout << "Hello world! "<< retValue << std::endl;
				
		}

	}

}