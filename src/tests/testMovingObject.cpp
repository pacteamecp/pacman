#include "tests.h"
//#include <gtest.h>

void test_movingObjects(sf::RenderWindow &window, sf::Event &event)
{
	Scene gameScene(window);
	gameScene.loadFromXML("../maps/map1.xml");
	
	while (window.isOpen())
	{
		while (window.pollEvent(event))
		{
			// If the red cross is pressed, close the window (and exit the game)
			if (event.type == sf::Event::Closed)
				window.close();
			else {
				gameScene.handleEvent(event);
			}
		}
		gameScene.play();
		gameScene.show();

		
	}
}