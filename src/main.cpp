/*!
* \file    main.cpp
* \author  Celia Amegavie, Arthur de la Barre de Nanteuil, Franck Pagny, Antoine Radet
*
* Jeu de Pacman développé pour le cours de PAV/MOD
*/

#include "GameManager.h"

int main()
{
	std::shared_ptr<GameManager> pacmanGame = std::make_shared<GameManager>("Pacman ECP");

	pacmanGame->play();

	return 0;
}
