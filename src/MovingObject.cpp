#include <iostream>
#include <Math.h>

#include "MovingObject.h"
#include "common.h"

MovingObject::MovingObject()
	: Entity(), _dir(NONE), _hp(0), _speed(0), _state(ALIVE)
{
	_baseSpeed = _speed;
	_nextDir = _dir;

	_killEvent = std::make_shared<GameEvent>();
}

MovingObject::MovingObject(float x0, float y0, float hb, VectPtrTexture textVect, Direction dir, int hp, float speed)
	: Entity(x0, y0, hb, textVect), _startCoord(x0, y0), _dir(dir), _hp(hp), _state(ALIVE)
{
	_speed = std::min(SPEED_MAX, speed);
	_baseSpeed = _speed;
	_lowSpeed = _speed;
	_nextDir = _dir;

	update_spriteNumber();

	_killEvent = std::make_shared<GameEvent>();
}

MovingObject::~MovingObject()
{}

void MovingObject::update_spriteNumber()
{
	_spriteNumber = 0;
}

void MovingObject::setDirection(Direction d)
{
	_dir = d;
	sf::Vector2f pos(_spriteVect[_spriteNumber].getPosition());

	update_spriteNumber();

	if (_dir != NONE)
	{
		_spriteVect[_spriteNumber].setPosition(pos.x, pos.y);
	}
	_spriteVect[NONE].setPosition(pos.x, pos.y);
}


void MovingObject::setNextDirection(Direction d)
{
	_nextDir = d;
}

void MovingObject::setState(int state)
{
	_state = state;
}

void MovingObject::setSpeed(float speed)
{
	_speed = std::min(SPEED_MAX, speed);
}

void MovingObject::setLowSpeed(float speed)
{
	_lowSpeed = speed;
}

Direction MovingObject::getDir()
{
	return _dir;
}

Direction MovingObject::getNextDir()
{
	return _nextDir;
}

int MovingObject::getState()
{
	return _state;
}

int MovingObject::getHp()
{
	return _hp;
}

float MovingObject::getSpeed()
{
	return _speed;
}

float MovingObject::getLowSpeed()
{
	return _lowSpeed;
}

float MovingObject::distance(Entity const &E, Direction dir)
{
	
	sf::Vector2f coordE = E.getCoord();
	float hitboxE       = E.getHitbox();

	float dist = _speed;

	switch (dir)
	{
	case NONE:
		//Ne rien faire
		break;
	case UP:
		if ((fabs((2 * _coord.x + _hitbox) - (2 * coordE.x + hitboxE)) < (_hitbox + hitboxE)))
		{
			dist = fabs((2 * _coord.y + _hitbox) - (2 * coordE.y + hitboxE)) - (_hitbox + hitboxE);
			dist = dist / 2;
		}
		break;

	case DOWN:
		if ((fabs((2 * _coord.x + _hitbox) - (2 * coordE.x + hitboxE)) < (_hitbox + hitboxE)))
		{
			dist = fabs((2 * _coord.y + _hitbox) - (2 * coordE.y + hitboxE)) - (_hitbox + hitboxE);
			dist = dist / 2;
		}
		break;

	case LEFT:
		if ((fabs((2 * _coord.y + _hitbox) - (2 * coordE.y + hitboxE)) < (_hitbox + hitboxE)))
		{
			dist = fabs((2 * _coord.x + _hitbox) - (2 * coordE.x + hitboxE)) - (_hitbox + hitboxE);
			dist = dist / 2;
		}
		break;

	case RIGHT:
		if ((fabs((2 * _coord.y + _hitbox) - (2 * coordE.y + hitboxE)) < (_hitbox + hitboxE)))
		{
			dist = fabs((2 * _coord.x + _hitbox) - (2 * coordE.x + hitboxE)) - (_hitbox + hitboxE);
			dist = dist / 2;
		}
		break;

	default:
		break;
	}
	return dist;
}


void MovingObject::move()
{
	switch (_dir)
	{
	case NONE:
		// Ne rien faire
		break;

	case UP:
		_coord.y = _coord.y + (-1)*_lowSpeed;
		if (_coord.y - GRID_SIZE * floor(_coord.y / GRID_SIZE) <= _lowSpeed)
		{
			_coord.y = GRID_SIZE * floor(_coord.y / GRID_SIZE);
		}
		if (_coord.y < Y_MIN) {
			_coord.y = (float)(Y_MAX - _hitbox);
		}
		break;

	case DOWN:
		_coord.y = _coord.y + _lowSpeed;
		if (_coord.y - GRID_SIZE * ceil(_coord.y / GRID_SIZE) >= - _lowSpeed)
		{
			_coord.y = GRID_SIZE * ceil(_coord.y / GRID_SIZE);
		}
		if (_coord.y >= Y_MAX) {
			_coord.y = (float)(Y_MIN);
		}
		break;

	case RIGHT:
		_coord.x = _coord.x + _lowSpeed;
		if (_coord.x - GRID_SIZE * ceil(_coord.x / GRID_SIZE) >= -_lowSpeed)
		{
			_coord.x = GRID_SIZE * ceil(_coord.x / GRID_SIZE);
		}
		if (_coord.x > X_MAX) {
			_coord.x = (float)(X_MIN);
		}
		break;

	case LEFT:
		_coord.x = _coord.x + (-1)*_lowSpeed;
		if (_coord.x - GRID_SIZE * floor(_coord.x / GRID_SIZE) <= _lowSpeed)
		{
			_coord.x = GRID_SIZE * floor(_coord.x / GRID_SIZE);
		}
		if (_coord.x < X_MIN) {
			_coord.x = (float)(X_MAX - _hitbox);
		}
		break;

	default:
		break;
	}
	
	_lowSpeed = _speed;
	_spriteVect[NONE].setPosition(_coord.x, _coord.y);
	_spriteVect[_spriteNumber].setPosition(_coord.x, _coord.y);
}

void MovingObject::respawn()
{
	setCoord(_startCoord.x, _startCoord.y);
	setDirection(RIGHT);
	setNextDirection(NONE);
}

void MovingObject::kill()
{
	_hp = _hp - 1;
	if (_hp > 0)
	{
		_state = ALIVE;
		_killEvent->trigger();
		respawn();
	}
	else {
		_state = DEAD;
	}
}

void MovingObject::setSpecialEffectTimer(int time)
{
	_specialEffectTimer = time;
}

bool MovingObject::updateSpecialEffectTimer()
{
	if (_specialEffectTimer > 0)
	{
		_specialEffectTimer--;
	}
	else
	{
		std::cout << "Timer has ended" << std::endl;
	}
	return (_specialEffectTimer <= 0);
}
