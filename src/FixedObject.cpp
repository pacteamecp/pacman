#include "FixedObject.h"


FixedObject::FixedObject()
	: Entity()
{
}


FixedObject::FixedObject(float x0, float y0, float hb, VectPtrTexture textVect)
	: Entity(x0, y0, hb, textVect)
{
}

FixedObject::~FixedObject()
{
}