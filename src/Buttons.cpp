#include "..\inc\Buttons.h"


Buttons::Buttons()
	: FixedObject()
{
}


Buttons::Buttons(float x0, float y0, float hb, VectPtrTexture textVect, int id)
	: FixedObject(x0, y0, hb, textVect), _buttonID(id)
{
}

Buttons::~Buttons()
{
}

int Buttons::getID()
{
	return _buttonID;
}


void Buttons::isFocus()
{
	if (!is_focused)
	{
		_spriteNumber = (_spriteNumber + 1) % 2;
		is_focused = true;
	}
	
}

void Buttons::deFocus()
{
	if (is_focused)
	{
		_spriteNumber = (_spriteNumber - 1) % 2;
		is_focused = false;
	}

}