#include <iostream>
#include <SFML/Graphics.hpp>

#include "Scene.h"
#include "MovingObject.h"
#include "Pacman.h"
#include "Wall.h"
#include "Ghost.h"
#include "Pacgum.h"
#include "Map.h"

using namespace sf;

Scene::Scene(sf::RenderWindow& win)
	: _renderWin(win)
{
	// Création de la map qui contiendra les gums et murs
	myMap = std::make_shared<Map>();
}

// Affiche la scène
void Scene::show()
{
	// Fond gris
	_renderWin.clear(sf::Color(145,145,145));

	myMap->draw(_renderWin);	// Murs et pacgums

	for (auto e : _vGhosts)
		e->draw(_renderWin);

	_pacman->draw(_renderWin);
	_statusBar->draw(_renderWin);

	_renderWin.display();
}

/**
 * Agit sur Pacman en fonction des événements clavier
 */
void Scene::handleEvent(sf::Event event)
{	
	//MovingObject *test = new MovingObject();
	switch (event.type)
	{
		case Event::KeyPressed: // Appui sur une touche
		{
			switch (event.key.code)
			{
			case sf::Keyboard::Left: // Touche left
				_pacman->setNextDirection(LEFT);
				break;

			case sf::Keyboard::Right: // Touche right
				_pacman->setNextDirection(RIGHT);
				break;

			case sf::Keyboard::Up: // Touche up
				_pacman->setNextDirection(UP);
				break;

			case sf::Keyboard::Down: // Touche down
				_pacman->setNextDirection(DOWN);
				break;

			default:
				break;
			}
		}
		break;

		default:
			break;
	}

}

void Scene::play()
{
	updatePacmanPacgum();
	updatePacmanGhost();
	updateNextDirToDir();

	for (auto g : _vGhosts)
	{
		/*
		if (abs((_pacman->getCoord()).x - (g->getCoord()).x) + abs((_pacman->getCoord()).y - (g->getCoord()).y) <= 200)
		{
		g->setMainStrategy(std::make_shared<Offensive>(), _pacman);
		}
		*/
		g->updateSpecialEffectTimer();
		g->applyStrategy();
	}

	updateGhostWall();

	for (auto g : _vGhosts) {
		g->move();
	}

	_pacman->move();
	_pacman->updateSpecialEffectTimer();
}

bool Scene::nextLevel()
{
	if (_nextLvlPath.empty() == false) {
		loadFromXML(_nextLvlPath);
		return true;
	}
	else {
		return false;
	}
}

void Scene::updatePacmanPacgum()
{
	std::vector<std::shared_ptr<FixedObject>> vPacgum;
	myMap->getGumAround(_pacman->getCoord(), vPacgum);

	for (auto &pg : vPacgum) {
		pg->collide(*_pacman, DOWN);
	}
}

void Scene::updatePacmanGhost()
{
	for (auto &g : _vGhosts)
	{
		if (g->collide(*_pacman, NONE))
		{
			if (g->getState() == ALIVE)
			{
				_pacman->kill();
				g->respawn();
			}
			if (g->getState() == SCARED)
			{
				g->setState(DEAD);
				g->respawn();
				g->setState(ALIVE);
			}
		}
	}
}

void Scene::updateNextDirToDir()
{
	collideWall(*_pacman);
}

void Scene::updateGhostWall()
{
	for (auto &g : _vGhosts) {
		collideWall(*g);
	}
}

void Scene::collideWall(MovingObject& mo)
{
	// Liste des murs
	std::vector<std::shared_ptr<FixedObject>> vWall;
	myMap->getWallAround(mo.getCoord(), vWall);
	float minDistanceDir = mo.getSpeed();
	//float minDistanceNextDir = mo.getSpeed();
	Direction dir = mo.getDir();
	if (dir != NONE)
	{
		for (auto &w : vWall)
		{
			// S'il n'est pas possible de se déplacer dans la
			// direction actuelle, on s'arrête
			if (mo.collide(*w, dir))
			{
				mo.setDirection(NONE);
				break;
			}
		}
	}

	Direction nextDir = mo.getNextDir();
	bool canTurn = true;

	if (nextDir != NONE)
	{
		for (auto &w : vWall)
		{
			// S'il y a un obstacle dans la direction suivante
			// on s'arrête
			if (mo.collide(*w, nextDir))
			{
				canTurn = false;
				break;
			}
		}
	}

	// S'il n'y a pas d'obstacle dans la direction suivante,
	// on prend cette direction
	if (canTurn)
	{
		mo.setDirection(nextDir);
	}

	// S'il n'est pas possible de se déplacer à la vitesse courante
	// dans la nouvelle direction choisismais qu'on est pas encore 
	// collé a mur, on avance plus lentement
	
	dir = mo.getDir();
	if (dir != NONE)
	{ 
		for (auto &w : vWall)
		{
			if ((mo.distance(*w, dir) < minDistanceDir) & (mo.collide(*w, dir) != true))
			{
				minDistanceDir = mo.distance(*w, dir);
				if (minDistanceDir != 0)
				{
					mo.setLowSpeed(minDistanceDir);
				}
			}
		}
	}
}