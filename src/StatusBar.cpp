#include <iostream>
#include "StatusBar.h"

StatusBar::StatusBar(std::shared_ptr<Pacman> pacman, std::string levelName)
	: _pacman(pacman)
{
	if (!_textLife.loadFromFile("../img/pacmanL.png"))
		std::cout << "Impossible de charger la texture pacmanL.png (points de vie)" << std::endl;
	else
		_spriteLife.setTexture(_textLife);

	if (!_scoringFont.loadFromFile("../img/ARESSENCE.ttf"))
		std::cout << "Impossible de charger la police de caracteres ARESSENCE.ttf" << std::endl;
	else
	{
		_score.setFont(_scoringFont);
		_level.setFont(_scoringFont);
	}

	_point = 0;

	// Positionnement au milieu du titre du niveau
	_level.setPosition(((float)WIN_SIZE_X / 2) - (levelName.length() * 6.0f), 8);
	_level.setString(levelName);
}


StatusBar::~StatusBar()
{
}

void StatusBar::draw(sf::RenderWindow &_renderWin)
{
	float pos = 30.0;
	int hp = _pacman->getHp();
	for (int i = 0; i < hp; i++)
	{
		_spriteLife.setPosition(pos, 5);
		pos += 30;
		_renderWin.draw(_spriteLife);
	}
	_score.setPosition(400, 5);
	_score.setString(std::to_string(_point));
	_renderWin.draw(_score);
	_renderWin.draw(_level);
}

void StatusBar::update(int arg)
{
	switch (arg) {
	case ID_GUM_EATEN:
		_point += 10;
		break;
	case ID_PEPPER_EATEN:
		_point += 50;
		break;
	case ID_CHERRY_EATEN:
		_point += 20;
		break;
	case ID_GHOST_KILLED:
		_point += 100;
		break;
	case ID_PACMAN_KILLED:
		_point -= 150;
		break;
	default:
		std::cout << "Error: [StatusBar] Unhandled event" << std::endl;
		break;
	}
}