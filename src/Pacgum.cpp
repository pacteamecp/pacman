#include "Pacgum.h"

Pacgum::Pacgum(float x0, float y0, float hb, VectPtrTexture textVect, PacgumType gumType)
	: FixedObject(x0, y0, hb, textVect), _type(gumType), _isEaten(false)
{
	_eatenEvent = std::make_shared<GameEvent>();

	switch (gumType){
	case GUM_BASIC:
		_eatenEvent->create(ID_GUM_EATEN);
		break;
	case GUM_CHERRY:
		_eatenEvent->create(ID_CHERRY_EATEN);
		break;
	case GUM_PEPPER:
		_eatenEvent->create(ID_PEPPER_EATEN);
		break;
	default:
		break;
	}
}

Pacgum::~Pacgum()
{
}

void Pacgum::setEaten()
{
	if (!_isEaten) {
		_isEaten = true;
		_eatenEvent->trigger();
	}
}

bool Pacgum::getEaten()
{
	return _isEaten;
}

PacgumType Pacgum::getPacgumType()
{
	return _type;
}

void Pacgum::draw(sf::RenderWindow &win)
{
	if (!_isEaten)
	{
		win.draw(_spriteVect[_spriteNumber]);
	}
}

void Pacgum::collideCallback(bool isTouched)
{
	if (isTouched)
		setEaten();
}