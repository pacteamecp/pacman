/**
 * \file SceneFactory.cpp
 * Fonctions de cr�ation de la sc�ne 
 *
 * \author Antoine RADET
 */

#include "Scene.h"
#include "gameEvent.h"

void Scene::clear()
{
	GameEvent::clear();
	_vGhosts.clear();
	_texVec.clear();
	myMap->reset();

	_lvlName.clear();
	_nextLvlPath.clear();
}

void Scene::loadFromXML(const std::string filename)
{
	clear();

	_lvlName = filename;

	TiXmlDocument mapFile(filename.c_str());
	mapFile.LoadFile();

	TiXmlHandle hDoc(&mapFile);
	TiXmlElement* pElem = hDoc.FirstChildElement().Element();
	TiXmlHandle hRoot = TiXmlHandle(pElem);	// R�cup�ration du noeud racine (<map>)

	// R�cup�ration du premier �l�ment du bloc <level> (propri�t�s du niveau)
 	pElem = hRoot.FirstChild("level").FirstChild().Element();
	initLevelProperties(pElem);

	// R�cup�ration du premier �l�ment du bloc <texture>
	pElem = hRoot.FirstChild("texture").FirstChild().Element();
	VectPtrTexture texPacman;
	VectPtrTexture texGhost;
	VectPtrTexture texWall;
	VectPtrTexture texGum;
	loadTexture(pElem, texPacman, texGhost, texWall, texGum);

	// R�cup�ration du premier �l�ment du bloc <path>
	pElem = hRoot.FirstChild("path").FirstChild().Element();
	initWall(pElem, texWall);

	// R�cup�ration du premier �l�ment du bloc <gum>
	pElem = hRoot.FirstChild("pacgum").FirstChild().Element();
	initGum(pElem, texGum);

	// R�cup�ration du premier �l�ment du bloc <movingObj>
	pElem = hRoot.FirstChild("movingObj").FirstChild().Element();
	// Cr�ation des fant�mes et du joueur
	while (pElem) {
		std::string movObjType = pElem->Value();
		
		if (movObjType.find("player") != std::string::npos)
			initPacman(pElem, texPacman);
		else if (movObjType.find("ghost") != std::string::npos)
			initGhost(pElem, texGhost);
		else
			std::cout << "Error: unknown type of moving object " << movObjType << std::endl;

		pElem = pElem->NextSiblingElement();
	}

	// Cr�ation de la barre de statut et inscription aux �v�nements des pacgum mang�es
	_statusBar = std::make_shared<StatusBar>(_pacman, _lvlName) ;
	GameEvent::listener_register(ID_GUM_EATEN,    _statusBar);
	GameEvent::listener_register(ID_CHERRY_EATEN, _statusBar);
	GameEvent::listener_register(ID_PEPPER_EATEN, _statusBar);
	GameEvent::listener_register(ID_GHOST_KILLED, _statusBar);
	GameEvent::listener_register(ID_PACMAN_KILLED, _statusBar);

	// Inscription de la map aux �v�nements des pacgums
	GameEvent::listener_register(ID_GUM_EATEN,    myMap);
	GameEvent::listener_register(ID_CHERRY_EATEN, myMap);
	GameEvent::listener_register(ID_PEPPER_EATEN, myMap);
}

void Scene::initLevelProperties(TiXmlElement* pXmlLvl)
{
	std::string sName, sPath;

	while (pXmlLvl)
	{
		sName = std::string(pXmlLvl->Value());

		if (sName.find("currentLevel") != std::string::npos) {
			_lvlName = std::string(pXmlLvl->Attribute("name"));
			pXmlLvl->QueryIntAttribute("levelNbr", &_lvlNbr);
		}
		else if (sName.find("nextLevel") != std::string::npos) {
			_nextLvlPath = std::string(pXmlLvl->Attribute("path"));
		}
		else
			std::cout << "Error: unknown type of level property " << sName << std::endl;

		pXmlLvl = pXmlLvl->NextSiblingElement();
	}
}

// Lecture des chemins d'acc�s aux images de texture, chargement des textures et ajout dans le vecteur de texture appropri�
void Scene::loadTexture(TiXmlElement* pXmlTex, VectPtrTexture& texPac, VectPtrTexture& texGh, VectPtrTexture& texWall, VectPtrTexture& texGum)
{
	std::string sName, sPath;
	std::shared_ptr<sf::Texture> newTex;

	sName = std::string(pXmlTex->Value());
	// La premi�re texture de la liste doit TOUJOURS �tre la texture par d�faut
	if (sName.find("DefaultTexture") == std::string::npos) {
		std::cout << "FATAL ERROR: The first texture must be the default texture\n\t Program Aborting...\n";
		system("PAUSE");
		exit(EXIT_FAILURE);
	}
	std::string sDefaultPath = std::string(pXmlTex->Attribute("path"));

	// Tentative de chargement de la texture par d�faut
	newTex = std::make_shared<sf::Texture>();
	if (newTex->loadFromFile(sDefaultPath) == false) {
		std::cout << "FATAL ERROR: Unable to load the default texture " << sDefaultPath << "\n\t Program Aborting...\n";
		system("PAUSE");
		exit(EXIT_FAILURE);
	}
	pXmlTex = pXmlTex->NextSiblingElement();

	while (pXmlTex)
	{
		sName = std::string(pXmlTex->Value());
		sPath = std::string(pXmlTex->Attribute("path"));

		newTex = std::make_shared<sf::Texture>();

		if (newTex->loadFromFile(sPath) == false)
		{
			std::cout << "Unable to load texture " << sPath << std::endl;
			// Chargement de la texture par d�faut
			newTex->loadFromFile(sDefaultPath);
		}
		_texVec.push_back(newTex);
		if (sName.find("Pacman") != std::string::npos)
			texPac.push_back(newTex);				
		else if (sName.find("Ghost") != std::string::npos)
			texGh.push_back(newTex);
		else if (sName.find("Wall") != std::string::npos)
			texWall.push_back(newTex);
		else if (sName.find("Pacgum") != std::string::npos)
			texGum.push_back(newTex);
		else
			std::cout << "Error: unknown type of texture " << sName << std::endl;

		pXmlTex = pXmlTex->NextSiblingElement();
	}

}

/**
 * Lecture du bloc <path> et cr�ation des murs en fonction des descriptions
 * Remarque : La carte est sym�trique par rapport � l'axe vertical
 */
void Scene::initWall(TiXmlElement* pXmlWall, const VectPtrTexture& texWall)
{
	int xStart, yStart, xEnd, yEnd;
	std::string sWallType;
	VectPtrTexture texH, texHe, texHs, texV, texVe, texVs, texC1, texC2, texC3, texC4;

	texH.push_back(texWall[WALL_H]);
	texHe.push_back(texWall[WALL_HE]);
	texHs.push_back(texWall[WALL_HS]);
	texV.push_back(texWall[WALL_V]);
	texVe.push_back(texWall[WALL_VE]);
	texVs.push_back(texWall[WALL_VS]);
	texC1.push_back(texWall[CORNER_UL]);	
	texC2.push_back(texWall[CORNER_UR]);	
	texC3.push_back(texWall[CORNER_BR]);	
	texC4.push_back(texWall[CORNER_BL]);

	while (pXmlWall)
	{
		// Lecture du type de mur (carr�, ligne,...)
		sWallType = std::string(pXmlWall->Value());

		// Lecture des coordonn�es de d�part
		pXmlWall->QueryIntAttribute("sx", &xStart);
		pXmlWall->QueryIntAttribute("sy", &yStart);

		if (sWallType.find("line") != std::string::npos) {
			switch (sWallType[4])
			{
			case 'H':	// Ligne horizontale
				pXmlWall->QueryIntAttribute("ex", &xEnd);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*xStart, GRID_SIZE*yStart, 20, texHs), MAP_WALL);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - xStart), GRID_SIZE*yStart, 20, texHe), MAP_WALL);
				for (int i = xStart+1; i <= xEnd-1; i++) {
					myMap->add(std::make_shared<Wall>(GRID_SIZE*i, GRID_SIZE*yStart, 20, texH), MAP_WALL);
					myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - i), GRID_SIZE*yStart, 20, texH), MAP_WALL);
				}
				myMap->add(std::make_shared<Wall>(GRID_SIZE*xEnd, GRID_SIZE*yStart, 20, texHe), MAP_WALL);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - xEnd), GRID_SIZE*yStart, 20, texHs), MAP_WALL);
				break;

			case 'V':	// Ligne verticale
				pXmlWall->QueryIntAttribute("ey", &yEnd);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*xStart, GRID_SIZE*yStart, 20, texVs), MAP_WALL);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - xStart), GRID_SIZE*yStart, 20, texVs), MAP_WALL);
				for (int j = yStart+1; j <= yEnd-1; j++) {
					myMap->add(std::make_shared<Wall>(GRID_SIZE*xStart, GRID_SIZE*j, 20, texV), MAP_WALL);
					myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - xStart), GRID_SIZE*j, 20, texV), MAP_WALL);
				}
				myMap->add(std::make_shared<Wall>(GRID_SIZE*xStart, GRID_SIZE*yEnd, 20, texVe), MAP_WALL);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - xStart), GRID_SIZE*yEnd, 20, texVe), MAP_WALL);
				break;

			default:
				std::cout << "Error: unknown line " << sWallType << std::endl;
				break;
			}
		}

		// Carr�
		else if (sWallType.find("square") != std::string::npos) {
			pXmlWall->QueryIntAttribute("ex", &xEnd);
			pXmlWall->QueryIntAttribute("ey", &yEnd);

			// Les 4 angles
			myMap->add(std::make_shared<Wall>(GRID_SIZE*xStart, GRID_SIZE*yStart, 20, texC1), MAP_WALL);
			myMap->add(std::make_shared<Wall>(GRID_SIZE*xEnd, GRID_SIZE*yStart, 20, texC2), MAP_WALL);
			myMap->add(std::make_shared<Wall>(GRID_SIZE*xEnd, GRID_SIZE*yEnd, 20, texC3), MAP_WALL);
			myMap->add(std::make_shared<Wall>(GRID_SIZE*xStart, GRID_SIZE*yEnd, 20, texC4), MAP_WALL);

			myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - xStart), GRID_SIZE*yStart, 20, texC2), MAP_WALL);
			myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - xEnd), GRID_SIZE*yStart, 20, texC1), MAP_WALL);
			myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - xEnd), GRID_SIZE*yEnd, 20, texC4), MAP_WALL);
			myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - xStart), GRID_SIZE*yEnd, 20, texC3), MAP_WALL);

			// Murs haut et bas
			for (int i = xStart + 1; i <= xEnd - 1; i++) {
				myMap->add(std::make_shared<Wall>(GRID_SIZE*i, GRID_SIZE*yStart, 20, texH), MAP_WALL);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*i, GRID_SIZE*yEnd, 20, texH), MAP_WALL);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - i), GRID_SIZE*yStart, 20, texH), MAP_WALL);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - i), GRID_SIZE*yEnd, 20, texH), MAP_WALL);
			}

			// Murs gauche et droite
			for (int j = yStart + 1; j <= yEnd - 1; j++) {
				myMap->add(std::make_shared<Wall>(GRID_SIZE*xStart, GRID_SIZE*j, 20, texV), MAP_WALL);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*xEnd, GRID_SIZE*j, 20, texV), MAP_WALL);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - xStart), GRID_SIZE*j, 20, texV), MAP_WALL);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - xEnd), GRID_SIZE*j, 20, texV), MAP_WALL);
			}
		}

		// Coins seuls
		else if (sWallType.find("corner") != std::string::npos) {
			switch (sWallType[6])
			{
			case '1':
				myMap->add(std::make_shared<Wall>(GRID_SIZE*xStart, GRID_SIZE*yStart, 20, texC1), MAP_WALL);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - xStart), GRID_SIZE*yStart, 20, texC2), MAP_WALL);
				break;

			case '2':
				myMap->add(std::make_shared<Wall>(GRID_SIZE*xStart, GRID_SIZE*yStart, 20, texC2), MAP_WALL);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - xStart), GRID_SIZE*yStart, 20, texC1), MAP_WALL);
				break;

			case '3':
				myMap->add(std::make_shared<Wall>(GRID_SIZE*xStart, GRID_SIZE*yStart, 20, texC3), MAP_WALL);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - xStart), GRID_SIZE*yStart, 20, texC4), MAP_WALL);
				break;

			case '4':
				myMap->add(std::make_shared<Wall>(GRID_SIZE*xStart, GRID_SIZE*yStart, 20, texC4), MAP_WALL);
				myMap->add(std::make_shared<Wall>(GRID_SIZE*(GRID_MAX_X - xStart), GRID_SIZE*yStart, 20, texC3), MAP_WALL);
				break;

			default:
				std::cout << "Error: unknown corner " << sWallType << std::endl;
				break;
			}
		}
			
		else {
			std::cout << "Error: unknown type of wall " << sWallType << std::endl;
		}
		
		pXmlWall = pXmlWall->NextSiblingElement();

	}
}

/**
* - Lecture du bloc <gum> et cr�ation des pacgum en fonction de la description
* - La carte est sym�trique par rapport � l'axe vertical
*/
void Scene::initGum(TiXmlElement* pXmlGum, const VectPtrTexture& texGum)
{
	int xStart, yStart, xEnd, yEnd;
	std::string sGumType;
	VectPtrTexture texStd, texCherry, texPepper;

	texStd.push_back(texGum[INDEX_GUM_STD]);
	texCherry.push_back(texGum[INDEX_GUM_CHERRY]);
	texPepper.push_back(texGum[INDEX_GUM_PEPPER]);

	while (pXmlGum)
	{
		// Lecture du type de mur (carr�, ligne,...)
		sGumType = std::string(pXmlGum->Value());

		// Lecture des coordonn�es de d�part
		pXmlGum->QueryIntAttribute("sx", &xStart);
		pXmlGum->QueryIntAttribute("sy", &yStart);

		if (sGumType.find("std") != std::string::npos) {
			switch (sGumType[3])
			{
			case 'H':	// Ligne horizontale
				pXmlGum->QueryIntAttribute("ex", &xEnd);
				for (int i = xStart; i <= xEnd; i++) {
					myMap->add(std::make_shared<Pacgum>(GRID_SIZE*i, GRID_SIZE*yStart, 10, texStd), MAP_GUM);
					if((GRID_MAX_X - i) != i) // Ne pas mettre 2 gums au m�me endroit (pour les murs ce test est inutile)
						myMap->add(std::make_shared<Pacgum>(GRID_SIZE*(GRID_MAX_X - i), GRID_SIZE*yStart, 10, texStd), MAP_GUM);
				}
				break;

			case 'V':	// Ligne verticale
				pXmlGum->QueryIntAttribute("ey", &yEnd);
				for (int j = yStart; j <= yEnd; j++) {
					myMap->add(std::make_shared<Pacgum>(GRID_SIZE*xStart, GRID_SIZE*j, 10, texStd), MAP_GUM);
					if ((GRID_MAX_X - xStart) != xStart) // Ne pas mettre 2 gums dans la m�me colonne
						myMap->add(std::make_shared<Pacgum>(GRID_SIZE*(GRID_MAX_X - xStart), GRID_SIZE*j, 10, texStd), MAP_GUM);
				}
				break;

			default:
				std::cout << "Error: unknown standard pacgum " << sGumType << std::endl;
				break;
			}

		}

		else if (sGumType.find("cherry") != std::string::npos) {
			myMap->add(std::make_shared<Pacgum>(GRID_SIZE*xStart, GRID_SIZE*yStart, 13, texCherry, GUM_CHERRY), MAP_GUM);
			if ((GRID_MAX_X - xStart) != xStart)
				myMap->add(std::make_shared<Pacgum>(GRID_SIZE*(GRID_MAX_X - xStart), GRID_SIZE*yStart, 13, texCherry, GUM_CHERRY), MAP_GUM);
		}

		else if (sGumType.find("pepper") != std::string::npos) {
			myMap->add(std::make_shared<Pacgum>(GRID_SIZE*xStart, GRID_SIZE*yStart, 12, texPepper, GUM_PEPPER), MAP_GUM);
			if ((GRID_MAX_X - xStart) != xStart)
				myMap->add(std::make_shared<Pacgum>(GRID_SIZE*(GRID_MAX_X - xStart), GRID_SIZE*yStart, 12, texPepper, GUM_PEPPER), MAP_GUM);
		}

		else {
			std::cout << "Error: unknown type of pacgum " << sGumType << std::endl;
		}

		pXmlGum = pXmlGum->NextSiblingElement();
	}
}

// Cr�ation de l'objet pacman et initialisation (textures, point de d�part, points de vie)
void Scene::initPacman(TiXmlElement* pXmlPac, const VectPtrTexture& texPac)
{
	float xStart=1, yStart=3, speed=1;
	int hp=3, hitBox=GRID_SIZE;
	
	pXmlPac->QueryFloatAttribute("sx", &xStart);
	pXmlPac->QueryFloatAttribute("sy", &yStart);
	pXmlPac->QueryIntAttribute("hp", &hp);
	pXmlPac->QueryIntAttribute("hitbox", &hitBox);
	pXmlPac->QueryFloatAttribute("speed", &speed);

	_pacman = std::make_shared<Pacman>(xStart*GRID_SIZE, yStart*GRID_SIZE, hitBox, texPac, NONE, hp, speed);
	GameEvent::listener_register(ID_PEPPER_EATEN, _pacman);
}

// Idem initPacman mais pour les fant�mes
void Scene::initGhost(TiXmlElement* pXmlGh, const VectPtrTexture& texGh)
{
	float xStart = 1, yStart = 3, speed = 1;
	int hitBox = GRID_SIZE;

	const char* ghostStr = pXmlGh->Value();
	int ghostNbr = 0;
	sscanf_s(ghostStr, "ghost%d", &ghostNbr);
	VectPtrTexture texGhCustom;
	texGhCustom.push_back(texGh[0]);
	texGhCustom.push_back(texGh[ghostNbr]);

	pXmlGh->QueryFloatAttribute("sx", &xStart);
	pXmlGh->QueryFloatAttribute("sy", &yStart);
	pXmlGh->QueryIntAttribute("hitbox", &hitBox);
	pXmlGh->QueryFloatAttribute("speed", &speed);

	std::shared_ptr<Ghost> newGhost = std::make_shared<Ghost>(xStart*GRID_SIZE, yStart*GRID_SIZE, hitBox, texGhCustom, LEFT, 0, speed, _pacman);

	std::string sStrategy = pXmlGh->Attribute("strategy");
	if (sStrategy.find("random") != std::string::npos) {
		newGhost->setMainStrategy(std::shared_ptr<Strategy>(new Random), _pacman);
	}
	else if (sStrategy.find("track") != std::string::npos) {
		newGhost->setMainStrategy(std::shared_ptr<Strategy>(new Track), _pacman);
	}
	else if (sStrategy.find("offensive") != std::string::npos) {
		newGhost->setMainStrategy(std::shared_ptr<Strategy>(new Offensive), _pacman);
	}
	else {
		std::cout << "Error: unknown type of strategy " << sStrategy << std::endl;
	}

	GameEvent::listener_register(ID_CHERRY_EATEN, newGhost);
	_vGhosts.push_back(newGhost);
}

