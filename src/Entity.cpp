#include "Entity.h"
#include <iostream>

int Entity::cptEntity = 1;

Entity::Entity()
	: _coord(0, 0), _hitbox(0), _spriteVect(), _spriteNumber(0)
{
	cptEntity++;
}

Entity::Entity(float x0, float y0, float hb, VectPtrTexture textVect)
	: _coord(x0, y0), _hitbox(hb), _spriteNumber(0)
{
	sf::Sprite sprite;
	for (auto t : textVect)
	{
		sprite.setTexture(*t);
		sprite.setPosition(x0, y0);
		_spriteVect.push_back(sprite);
	}
	cptEntity++;
}

Entity::~Entity()
{
	cptEntity--;
}

sf::Vector2f Entity::getCoord() const
{
	return _coord;
}
void Entity::setCoord(float x, float y)
{
	_coord.x = x;
	_coord.y = y;
}

void Entity::set_spriteNumber(int spriteNumber)
{
	_spriteNumber = spriteNumber;
}

void Entity::draw(sf::RenderWindow &win)
{
	win.draw(_spriteVect[_spriteNumber]);
}

float Entity::getHitbox() const
{
	return _hitbox;
}

bool Entity::collide(Entity const &E, Direction dir)
{
	bool temp = false;
	float distance = 0.1f;
	switch (dir)
	{
	case NONE:
	{
		temp = ((fabs((2 * _coord.x + _hitbox) - (2 * E._coord.x + E._hitbox)) < (_hitbox + E._hitbox))
			&& (fabs((2 * _coord.y + _hitbox) - (2 * E._coord.y + E._hitbox)) < (_hitbox + E._hitbox)));
		break;
	}
	case UP:
	{
		temp = ((fabs((2 * _coord.x + _hitbox) - (2 * E._coord.x + E._hitbox)) < (_hitbox + E._hitbox))
			&& (fabs((2 * (_coord.y - distance) + _hitbox) - (2 * E._coord.y + E._hitbox)) < (_hitbox + E._hitbox)));
		break;
	}
	case DOWN:
	{
		temp = ((fabs((2 * _coord.x + _hitbox) - (2 * E._coord.x + E._hitbox)) < (_hitbox + E._hitbox))
			&& (fabs((2 * (_coord.y + distance) + _hitbox) - (2 * E._coord.y + E._hitbox)) < (_hitbox + E._hitbox)));
		break;
	}
	case LEFT:
	{
		temp = ((fabs((2 * (_coord.x - distance) + _hitbox) - (2 * E._coord.x + E._hitbox)) < (_hitbox + E._hitbox))
			&& (fabs((2 * _coord.y + _hitbox) - (2 * E._coord.y + E._hitbox)) < (_hitbox + E._hitbox)));
		break;
	}
	case RIGHT:
	{
		temp = ((fabs((2 * (_coord.x + distance) + _hitbox) - (2 * E._coord.x + E._hitbox)) < (_hitbox + E._hitbox))
			&& (fabs((2 * _coord.y + _hitbox) - (2 * E._coord.y + E._hitbox)) < (_hitbox + E._hitbox)));
		break;
	}
	default:
		break;
	}

	collideCallback(temp);

	return temp;
}
