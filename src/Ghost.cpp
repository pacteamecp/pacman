#include "Ghost.h"
#include "gameEvent.h"

Ghost::Ghost()
	:MovingObject(), _mainStrategy(std::shared_ptr<Offensive> (new Offensive))
{
	_killEvent->create(ID_GHOST_KILLED);
}

Ghost::Ghost(float x0, float y0, float hb, VectPtrTexture textVect, Direction dir, int hp, float speed, std::shared_ptr<Pacman>pacman)
	: MovingObject(x0, y0, hb, textVect, dir, hp, speed), _mainStrategy(std::shared_ptr<Offensive>(new Offensive))
{
	_killEvent->create(ID_GHOST_KILLED);
}

Ghost::~Ghost()
{
}

void Ghost::update_spriteNumber()
{
	if (_state == SCARED)
		_spriteNumber = GHOST_SPRITE_SCARED;
	else
		_spriteNumber = GHOST_SPRITE_OK;
}

void Ghost::setStrategyTarget(std::shared_ptr<Pacman> target)
{
	_currentStrategy->setPacmanPtr(target);
}

void Ghost::setCurrentStrategy(std::shared_ptr < Strategy > strategy)
{
	std::shared_ptr<Pacman> pacman = _mainStrategy->getStrategyTarget();
	_currentStrategy = strategy;
	std::weak_ptr<Ghost> ghost = _mainStrategy->getStrategyGhost();
	_currentStrategy->setGhostPtr(ghost);
	setStrategyTarget(pacman);
}

void Ghost::setMainStrategy(std::shared_ptr < Strategy > strategy, std::shared_ptr<Pacman> pacman)
{
	_mainStrategy = strategy;
	_mainStrategy->setPacmanPtr(pacman);
	_mainStrategy->setGhostPtr(shared_from_this());
	setCurrentStrategy(strategy);
}

void Ghost::applyStrategy()
{
	setNextDirection(_currentStrategy->play());
}

bool Ghost::updateSpecialEffectTimer()
{
	if (_specialEffectTimer > 0)
	{
		_specialEffectTimer--;
	}
	else
	{
		_currentStrategy = _mainStrategy;
		setSpeed(_baseSpeed);
		if (_state != DEAD)
		{
			_state = ALIVE;
		}
		update_spriteNumber();
	}
	return (_specialEffectTimer <= 0);
}

void Ghost::kill()
{
	_state = ALIVE;
	_specialEffectTimer = 0;
	updateSpecialEffectTimer();
	_killEvent->trigger();
	respawn();
}

void Ghost::update(int arg)
{
	switch (arg)
	{
	case ID_CHERRY_EATEN:
		//std::cout << "CHERRY : RUN!!!" << std::endl;
		setState(SCARED);
		setSpeed(0.5f*_baseSpeed);
		setCurrentStrategy(std::shared_ptr<Afraid>(new Afraid));
		setSpecialEffectTimer(GHOST_SCARED_TIMEOUT);
		update_spriteNumber();
		break;

	default:
		std::cout << "Error : unhandled event : " << arg << std::endl;
		break;
	}
}

void Ghost::setSpecialEffectTimer(int time)
{
	_specialEffectTimer = time;
}
