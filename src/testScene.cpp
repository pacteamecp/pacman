#include "tests.h"

using namespace sf;
 
void test_scene(sf::RenderWindow &window, sf::Event &event)
{
	Scene gameScene(window);
	sf::Sprite sprite;
	std::vector<sf::Sprite> spriteV;
	std::shared_ptr<sf::Texture> textureN = std::make_shared<sf::Texture>();
	std::shared_ptr<sf::Texture> textureU= std::make_shared<sf::Texture>();
	std::shared_ptr<sf::Texture> textureD= std::make_shared<sf::Texture>();
	std::shared_ptr<sf::Texture> textureL= std::make_shared<sf::Texture>();
	std::shared_ptr<sf::Texture> textureR= std::make_shared<sf::Texture>();

	std::shared_ptr<sf::Texture> texturePG= std::make_shared<sf::Texture>();
	std::shared_ptr<sf::Texture> textureGN= std::make_shared<sf::Texture>();
	std::shared_ptr<sf::Texture> textureGU= std::make_shared<sf::Texture>();
	std::shared_ptr<sf::Texture> textureGD= std::make_shared<sf::Texture>();
	std::shared_ptr<sf::Texture> textureGL= std::make_shared<sf::Texture>();
	std::shared_ptr<sf::Texture> textureGR= std::make_shared<sf::Texture>();

	if (!textureN->loadFromFile("../img/pacmanN.png"))
	{
		std::cout << "Impossible de charger la texture" << std::endl;
	}
	if (!textureU->loadFromFile("../img/pacmanU.png"))
	{
		std::cout << "Impossible de charger la texture" << std::endl;
	}
	if (!textureD->loadFromFile("../img/pacmanD.png"))
	{
		std::cout << "Impossible de charger la texture" << std::endl;
	}
	if (!textureL->loadFromFile("../img/pacmanL.png"))
	{
		std::cout << "Impossible de charger la texture" << std::endl;
	}
	if (!textureR->loadFromFile("../img/pacmanR.png"))
	{
		std::cout << "Impossible de charger la texture" << std::endl;
	}
	if (!texturePG->loadFromFile("../img/PacGum.png"))
	{
		std::cout << "Impossible de charger la texture" << std::endl;
	}
	if (!textureGN->loadFromFile("../img/ghostN.png"))
	{
		std::cout << "Impossible de charger la texture" << std::endl;
	}
	if (!textureGU->loadFromFile("../img/ghostU.png"))
	{
		std::cout << "Impossible de charger la texture" << std::endl;
	}
	if (!textureGD->loadFromFile("../img/ghostD.png"))
	{
		std::cout << "Impossible de charger la texture" << std::endl;
	}
	if (!textureGL->loadFromFile("../img/ghostL.png"))
	{
		std::cout << "Impossible de charger la texture" << std::endl;
	}
	if (!textureGR->loadFromFile("../img/ghostR.png"))
	{
		std::cout << "Impossible de charger la texture" << std::endl;
	}
	VectPtrTexture textVect;
	VectPtrTexture textVect2;
	VectPtrTexture textVect3;
	textVect.push_back(textureN);
	textVect.push_back(textureU);
	textVect.push_back(textureD);
	textVect.push_back(textureL);
	textVect.push_back(textureR);
	textVect2.push_back(texturePG);
	textVect3.push_back(textureGN);
	textVect3.push_back(textureGU);
	textVect3.push_back(textureGD);
	textVect3.push_back(textureGL);
	textVect3.push_back(textureGR);

	
	

	Pacman Obj(50, 50, 20, textVect, DOWN, 0, 1);
	Wall Obj2(100, 100, 10, textVect2);
	Pacgum gum1(150, 150, 10, textVect2);
	Pacgum gum2(200, 200, 10, textVect2);
	Pacgum gum3(250, 250, 10, textVect2);
	//auto _pacman = std::make_shared<Pacman>(Obj);
	Ghost Ghost1(300, 300, 20, textVect3, RIGHT, 0, 1, std::make_shared<Pacman> (Obj));
	//Ghost1.setStrategy(std::shared_ptr<Strategy> (new Offensive));

	
	std::vector<Ghost*> listGhost;
	listGhost.push_back(& Ghost1);
	std::vector<Pacgum*> listGum;
	listGum.push_back(&gum1);
	listGum.push_back(&gum2);
	listGum.push_back(&gum3);

	while (window.isOpen())
	{
		while (window.pollEvent(event))
		{
			// If the red cross is pressed, close the window (and exit the game)
			if (event.type == sf::Event::Closed)
				window.close();
			else {
				//gameScene.handleEvent(event, Obj);
				gameScene.handleEvent(event);
			}
		}
		window.clear();
		Obj.draw(window);
		//gameScene.updatePacmanPacgum(listGum, Obj);
		//gameScene.updatePacmanGhost(listGhost, Obj);
		gum1.draw(window);
		gum2.draw(window);
		gum3.draw(window);
		Ghost1.applyStrategy();
		Ghost1.draw(window);
		Ghost1.move();
		/*if (!Obj.collide(Obj2))
		{
		Obj2.draw(window);
		}*/

		Obj.move();
		window.display();
		// Update the display
		//gameScene.show();
	}
}
