// General C++ headers
#include <iostream>

// SFML headers
#include <SFML/Graphics.hpp>

// Game specific headers
#include "GameManager.h"
#include "common.h"
#include "tests.h"
#include "Menu.h"

GameManager::GameManager(std::string name)
	: myRenderWin(sf::VideoMode(WIN_SIZE_X, WIN_SIZE_Y), name), myGameState(GAME_NOT_STARTED)
{
	myRenderWin.setFramerateLimit(60);
}

GameManager::~GameManager()
{
}

void GameManager::play()
{
	sf::Event event;
	PointerMouse userMouse;	// Souris de l'utilisateur
	Menu startMenu(MENU_START, userMouse);
	Menu pauseMenu(MENU_PAUSE, userMouse);
	int retMenu = BUTTON_START;

	Scene gameScene(myRenderWin);

	while (myRenderWin.isOpen())
	{
		switch (myGameState) {
		case GAME_NOT_STARTED:
		case GAME_OVER:
			myRenderWin.pollEvent(event);
			retMenu = startMenu.show(myRenderWin, event);
			switch (retMenu) {
			case BUTTON_QUIT:
				myGameState = GAME_NOT_STARTED;
				myRenderWin.close();
				break;

			case BUTTON_START:
				myGameState = GAME_CONTINUE;
				gameScene.loadFromXML("../maps/map0.xml");
				GameEvent::listener_register(ID_PACMAN_DEAD, shared_from_this());
				GameEvent::listener_register(ID_LEVEL_FINISHED, shared_from_this());
				break;
			}
			break;

		case GAME_RESTART:
			myGameState = GAME_CONTINUE;
			gameScene.loadFromXML("../maps/map0.xml");
			GameEvent::listener_register(ID_PACMAN_DEAD, shared_from_this());
			GameEvent::listener_register(ID_LEVEL_FINISHED, shared_from_this());
			break;

		case GAME_LEVEL_FINISHED:
			if (gameScene.nextLevel()) {
				GameEvent::listener_register(ID_PACMAN_DEAD, shared_from_this());
				GameEvent::listener_register(ID_LEVEL_FINISHED, shared_from_this());
				myGameState = GAME_CONTINUE;
			}
			else {
				myGameState = GAME_NOT_STARTED;
			}
			break;

		default:
			break;
		}

		while (myGameState == GAME_CONTINUE) {
			while (myRenderWin.pollEvent(event))
			{
				// Fermer la fen�tre quand on clique sur la croix rouge
				if (event.type == sf::Event::Closed) {
					myGameState = GAME_NOT_STARTED;
					myRenderWin.close();
				}
				// Lancer le menu de pause lorsque la barre d'espace est press�e
				else if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Space) {
					retMenu = pauseMenu.show(myRenderWin, event);

					switch (retMenu) {
					  case BUTTON_QUIT:
						  myGameState = GAME_OVER;
						  myRenderWin.close();
						  break;

					  case BUTTON_BACK_TO_START:
						  myGameState = GAME_RESTART;
						  break;

					  case BUTTON_RESUME:
						  myGameState = GAME_CONTINUE;
						  break;

					  default:
						break;
					}
				}
				// Sinon on joue
				else {
					// Envoi des �v�nements � la sc�ne
					gameScene.handleEvent(event);
				}
			}
			gameScene.play();
			gameScene.show();
		}
	}
}

void GameManager::update(int arg)
{
	switch (arg) {
	case ID_PACMAN_DEAD:
		myGameState = GAME_OVER;
		//std::cout << "Pacman is Dead..." << std::endl;
		break;

	case ID_LEVEL_FINISHED:
		myGameState = GAME_LEVEL_FINISHED;
		//std::cout << "Level Complete!" << std::endl;
		break;

	default:
		std::cout << "Error: [StatusBar] Unhandled event" << std::endl;
		break;
	}
}