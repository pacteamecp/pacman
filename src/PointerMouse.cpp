#include "..\inc\PointerMouse.h"

using namespace sf;

PointerMouse::PointerMouse()
	: MovingObject()
{
}


PointerMouse::~PointerMouse()
{
}


void PointerMouse::setPosition(sf::Vector2i position){
	mousePoz = position;
}

bool PointerMouse::Collide(sf::Vector2f coord, float hitbox_x){

	if (mousePoz.x < (coord.x + hitbox_x) && mousePoz.x>coord.x)
	{
		if (mousePoz.y < (coord.y + BUTTON_Y) && mousePoz.y > coord.y){
			return true;
		}

	}
	return false;
}
