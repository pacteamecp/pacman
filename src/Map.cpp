#include "Map.h"
#include <iostream>
#include <math.h>

Map::Map()
	: myGumCounter(0)
{
	for (int x = 0; x < NBR_MAX_X; x++)
	{
		for (int y = 0; y < NBR_MAX_Y; y++)
		{
			myContentType[x][y] = MAP_NONE;
		}
	}
}

void Map::add(std::shared_ptr<FixedObject> pObj, fixObjType t_obj)
{
	sf::Vector2f coord = pObj->getCoord();

	// Conversion de coordonn�es flotantes en cases enti�res
	// Ajout de 1 pour les lignes (colonne 0 = recopie de la derni�re colonne)
	int gridX = (int)ceil(coord.x / GRID_SIZE) + 1;
	// Soustraction de 1 (la ligne 0 est affich�e comme �tant la troisi�me ligne)
	int gridY = (int)ceil(coord.y / GRID_SIZE) - 1;

	// Ajout de l'objet dans les grilles 
	myContent[gridX][gridY]     = pObj;
	myContentType[gridX][gridY] = t_obj;

	// Recopie des �l�ments de la premi�re ou derni�re colonne
	if (gridX == 1) {
		myContent[NBR_MAX_X - 1][gridY]     = pObj;
		myContentType[NBR_MAX_X - 1][gridY] = t_obj;
	}
	else if (gridX == (NBR_MAX_X - 2)) {
		myContent[0][gridY] = pObj;
		myContentType[0][gridY] = t_obj;
	}

	// Recopie des �l�ments de la premi�re ou derni�re ligne
	if (gridY == 1) {
		myContent[gridX][NBR_MAX_Y - 1]     = pObj;
		myContentType[gridX][NBR_MAX_Y - 1] = t_obj;
	}
	else if (gridY == (NBR_MAX_Y - 2)) {
		myContent[gridX][0]     = pObj;
		myContentType[gridX][0] = t_obj;
	}

	if (t_obj == MAP_GUM) {
		myGumCounter++;
	}
}

void Map::reset()
{
	for (int i = 0; i < NBR_MAX_X; i++)
	{
		for (int j = 0; j < NBR_MAX_Y; j++)
		{
			myContentType[i][j] = MAP_NONE;
		}
	}
	// Inutile de vider les cases de myContent car l'objet sera supprim� d�s qu'il ne sera plus r�f�renc�

	myGumCounter = 0;

	noMoreGumEvent = std::make_shared<GameEvent>();
	noMoreGumEvent->create(ID_LEVEL_FINISHED);
}

void Map::getWallAround(sf::Vector2f &coord, std::vector<std::shared_ptr<FixedObject>>& vWall)
{
	int gridX = (int)ceil(coord.x / GRID_SIZE) + 1;
	int gridY = (int)ceil(coord.y / GRID_SIZE) - 1;

	int k = 0;

	// D'apres le code ci-dessous, il ne peut y avoir que 25 murs r�f�renc�s au maximum
	vWall.resize(25);

	int gridXi = std::max(0, gridX - 2);
	int gridYi = std::max(0, gridY - 2);
	int gridXf = std::min(NBR_MAX_X-1, gridX + 2);
	int gridYf = std::min(NBR_MAX_Y-1, gridY + 2);

	for (int i = gridXi; i <= gridXf; i++)
	{
		for (int j = gridYi; j <= gridYf; j++)
		{
			if (myContentType[i][j] == MAP_WALL) {
				vWall[k++] = myContent[i][j];
			}
		}
	}

	// Suppression des cases vides du vecteur de mur
	vWall.resize(k);
}

void Map::getGumAround(sf::Vector2f &coord, std::vector<std::shared_ptr<FixedObject>>& vGum)
{
	int gridX = (int)ceil(coord.x / GRID_SIZE) + 1;
	int gridY = (int)ceil(coord.y / GRID_SIZE) - 1;
	int k = 0;

	// L'allocation de la taille permet de passer d'�viter une surcharge CPU due � des push_back()
	// -> R�sultat : passage de 100% CPU � 0.8%
	vGum.resize(9);

	int gridXi = std::max(0, gridX - 1);
	int gridYi = std::max(0, gridY - 1);
	int gridXf = std::min(NBR_MAX_X - 1, gridX + 1);
	int gridYf = std::min(NBR_MAX_Y - 1, gridY + 1);
	
	for (int i = gridXi; i <= gridXf; i++)
	{
		for (int j = gridYi; j <= gridYf; j++)
		{
			if (myContentType[i][j] == MAP_GUM) {
				vGum[k++] = myContent[i][j];
			}
		}
	}
	vGum.resize(k);
}

void Map::draw(sf::RenderWindow &win)
{
	for (int i = 1; i < NBR_MAX_X-1; i++)
	{
		for (int j = 1; j < NBR_MAX_Y-1; j++)
		{
			if (myContentType[i][j] != MAP_NONE)
				myContent[i][j]->draw(win);
		}
	}
}

void Map::update(int arg)
{
	switch (arg) {
	case ID_GUM_EATEN:
	case ID_CHERRY_EATEN:
	case ID_PEPPER_EATEN:
		myGumCounter--;
		if (myGumCounter <= 0) {
			//std::cout << "No more gum..." << std::endl;
			noMoreGumEvent->trigger();
		}
		break;

	default:
		std::cout << "Error: [Map] Unhandled event" << std::endl;
		break;
	}
}