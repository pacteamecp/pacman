/*!
 * \file GameEvent.h
 * \author Antoine Radet
 *
 * \brief Ev�nement de jeu
 * Permet l'envoi d'informations entre objets pendant le d�roulement du jeu 
 * Utile pour pr�venir qu'une pacgum a �t� mang�e par exemple
 *
 * Utilisation :
 *	* Dans la classe qui g�n�re l'�v�nement
 *		- Cr�er un shared_ptr<GameEvent> pour chaque �v�nement g�n�r�
 *		- Dans le constructeur initialiser le shared_ptr (constructeur vide) puis appeler la fonction create()
 *
 *	* Dans la classe qui �coute l'�v�nement
 *		- La classe doit h�riter de GameEventListener
 *		- Red�finir la fonction update si n�cessaire (par d�faut elle affichera l'argument envoy� par l'�v�nement)
 */

#pragma once
#include "GameEventListener.h"

#include <vector>
#include <memory>

/* Un �v�nement appel la fonction gameEvent_callback des objets qui l'�coutent */
class GameEvent : public std::enable_shared_from_this<GameEvent>
{
private:
	// ID de l'�v�nement
	int myID; 
	
	// Liste des objets � l'�coute de l'�v�nement
	std::vector<std::shared_ptr<GameEventListener>> myListenerList;

	/* Liste de tous les �v�nements existants */
	static std::vector<std::shared_ptr<GameEvent>> eventList;

public:
	GameEvent();
	~GameEvent();

	int getID();

	/** Cr�ation d'un nouvel �v�nement (appel du constructeur n�cessaire avant) */
	bool create(int id);

	/** Lance l'�v�nement et passe (�v�ntuellement) une valeur en param�tre
	 * Cette valeur sera envoy�e � la fonction update() du listener
	 * Si aucun argument n'est pass� c'est l'ID de l'�v�nement qui est envoy�
	 */
	void trigger(int arg = 0);
	 
	/**
	 * Demande d'enregistrement aupr�s d'un �v�nement
	 * \param id_event : le type d'�v�nement
	 * \param listener : un shared_ptr sur l'objet � pr�venir en cas d'�v�nement
	 * \return False si l'�v�n�ment n'existe pas
	 */
	static bool listener_register(int id_event, std::shared_ptr<GameEventListener> listener);

	/**
	 * Suppression de tous les �v�nements ayant un ID donn�
	 */
	static void event_remove(int id_event);

	/**
	 * Suppression de tous les �v�nements cr��s
	 */
	static void clear();
};

