/*!
* \file MovingObject.h
* \author Arthur de Nanteuil, Antoine Radet, Franck Pagny
*
* \brief Un objet qui se d�place � l'�cran (pacman, fantome,...)
*/

#pragma once
#include "Entity.h"
#include "GameEvent.h"

#define DEAD 0
#define ALIVE 1

#define SPEED_MAX ((float)4.0)	/*!< Vitesse maximale autoris�e */

class MovingObject :
	public Entity
{
protected:
	sf::Vector2f _startCoord;	/*!< Position de d�part (position donn�e lors de la cr�ation)	*/
	Direction _dir;				/*!< Direction de d�placement									*/
	Direction _nextDir;			/*!< Direction suivante demand�e par le contr�leur de l'objet	*/
	int   _hp;					/*!< Points de vie								*/
	float _speed;				/*!< Vitesse de d�placement courante			*/
	float _baseSpeed;			/*!< Vitesse de d�placement de base				*/
	int   _state;				/*!< Etat courant de l'objet (mort, vivant,...) */
	int   _specialEffectTimer;	/*!< Dur�e restante de l'effet sp�cial appliqu� sur l'objet (en nombre de frames) */
	float   _lowSpeed;		    /*!< Vitesse de d�placement lorsqu'elle est inf�rieur � la vitesse courante	      */
	std::shared_ptr<GameEvent> _killEvent; /*!< Ev�nement lanc� lorsque l'objet est tu� 
											* Allou� dans le constructeur de MovingObject mais les classes 
											* filles doivent appeler create() dans leur constructeur */


	virtual void collideCallback(bool isTouched) = 0;
	
	/* Mise � jour de l'attribut _spriteNumber suivant les caract�ristiques de l'objet 
	 * Par d�faut met le sprite 0 */
	virtual void update_spriteNumber();

public:
	MovingObject();
	MovingObject(float x0, float y0, float hb, VectPtrTexture textVect, Direction dir, int hp, float speed);
	~MovingObject();

	/* Change la direction de l'objet */
	void setDirection(Direction );

	/* Change la prochaine direction de l'objet */
	void setNextDirection(Direction );

	/* Change l'�tat de l'objet */
	void setState(int);

	/* Change la vitesse de l'objet */
	void setSpeed(float);

	/* Change la vitesse lente de l'objet */
	void setLowSpeed(float);

	/* Renvoie la direction de l'objet */
	Direction getDir();

	/* Renvoie la prochaine direction de l'objet */
	Direction getNextDir();

	/* Renvoie l'�tat de l'objet */
	int getState();

	/* Renvoie le nombre de points de vie */
	int getHp(void);

	/* Renvoie la vitesse de l'objet */
	float getSpeed(void);

	/* Renvoie la vitesse lente l'objet */
	float getLowSpeed(void);

	/** Renvoie la plus petite distance entre l'objet courant et 
	 * l'objet E dans la direction dir */
	virtual float distance(Entity const &E, Direction dir);

	/* D�place l'object */
	virtual void move();

	/* Retour � la position d'origine */
	virtual void respawn();

	/* Perte d'une vie et respawn si point de vie positifs */
	virtual void kill();

	/* Modifie la dur�e de l'effet sp�cial appliqu� sur l'objet */
	virtual void setSpecialEffectTimer(int time);

	/* D�cr�mente le temps restant de l'effet sp�cial et indique s'il est termin� */
	virtual bool updateSpecialEffectTimer();
};