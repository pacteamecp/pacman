/*!
* \file PointerMouse.h
* \author Celia Amegavie
*
* \brief Pointeur de souris pour les menus
*/

#pragma once
#include "MovingObject.h"
#include "Buttons.h"




class PointerMouse
	:public MovingObject
{
private :
	void collideCallback(bool isTouched) {};

protected:
	sf::Vector2i mousePoz;

public:
	PointerMouse();
	//PointerMouse(float, float, int, bool, VectPtrTexture);
	~PointerMouse();
	
	/*recup�re la position de la souris*/
	void setPosition(sf::Vector2i);

	/*teste si on est sur un bouton*/
	bool Collide(sf::Vector2f coord, float hitbox_x);
};

