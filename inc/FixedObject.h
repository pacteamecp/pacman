/*!
* \file FixedObject.h
* \author Arthur de Nanteuil
*
* \brief Un objet qui ne bouge pas (ex.: mur, pacgum,...)
*/

#pragma once
#include "Entity.h"

class FixedObject :
	public Entity
{
protected:
	virtual void collideCallback(bool isTouched) = 0;

public:
	FixedObject();
	FixedObject(float, float, float, VectPtrTexture);
	~FixedObject();
};


