/*!
* \file Scene.h
* \author Arthur de Nanteuil
*
* \brief Une Scene gere le contenu de tout ce qui est affich� � l'�cran
* Elle gere les interactions entre entit�s et les d�placements de Pacman
*/

#pragma once

#include <string>
#include <memory>
#include <SFML\Graphics.hpp>
#include <tinyxml.h>
#include <algorithm>

#include "common.h"
#include "Pacgum.h"
#include "Pacman.h"
#include "Ghost.h"
#include "Wall.h"
#include "Map.h"
#include "StatusBar.h"

// Indice des diff�rentes textures de murs
// UL = Up Left, BR = Bottom Right, HE = Horizontal End
enum { WALL_H, WALL_HS, WALL_HE, WALL_V, WALL_VS, WALL_VE, CORNER_UL, CORNER_UR, CORNER_BR, CORNER_BL };

// Indice des diff�rentes textures de pacgum
enum { INDEX_GUM_STD, INDEX_GUM_CHERRY, INDEX_GUM_PEPPER };

class Scene  
{
private:
	sf::RenderWindow &_renderWin;	/*!< Fen�tre pour l'affichage des objets graphiques */
	
	std::shared_ptr<Map> myMap;						/* Ensemble des objets fixes (murs + pacgum) */
	std::vector<std::shared_ptr<Ghost>> _vGhosts;	/*!< Liste des fant�mes */
	std::shared_ptr<Pacman> _pacman;				/*!< Instance de Pacman */
	std::shared_ptr<StatusBar> _statusBar;			/*!< Vies restants, compteur de points,... */

	VectPtrTexture _texVec;	/*!< Liste des textures utilis�es */

	/** Propri�t�s du niveau */
	std::string _lvlName;
	int _lvlNbr;
	std::string _nextLvlPath;

	/** Fonctions internes utilis�es pour le chargement de la carte */
	void initLevelProperties(TiXmlElement* pXmlLvl);
	void loadTexture(TiXmlElement* pXmlTex, VectPtrTexture& texPac, VectPtrTexture& texGh, VectPtrTexture& texWall, VectPtrTexture& texGum);
	void initWall(TiXmlElement* pXmlWall, const VectPtrTexture& texWall);
	void initGum(TiXmlElement* pXmlWall, const VectPtrTexture& texGum);
	void initPacman(TiXmlElement* pXmlPac, const VectPtrTexture& texPac);
	void initGhost(TiXmlElement* pXmlGh, const VectPtrTexture& texGh);

	/** Effacement des vecteurs, de la map, etc... */
	void clear();

	/** Fonctions de collisions de Pacman */
	void updatePacmanPacgum();
	void updatePacmanGhost();
	void updatePacmanWall();
	void updateNextDirToDir();

	/** Test de collision des fantomes contre les murs */
	void updateGhostWall();

	// Test si l'objet peut se d�placer sans toucher de mur et met � jour ses attributs de direction en fonction du r�sultat
	void collideWall(MovingObject& mo);

public:
	Scene(sf::RenderWindow&);

	/** Affiche tout le contenu de la sc�ne */
	void show();
	
	/** Analyse les �v�nements et modifie �ventuellement l'�tat du Pacman en fonction */
	void handleEvent(sf::Event event);

	/** Charge les �l�ments de la sc�ne en fonction d'un fichier de description XML */
	void loadFromXML(const std::string filename);

	/** Charge le niveau suivant s'il y en a un, sinon retourne False */
	bool nextLevel();

	/** Mise � jour de l'�tat des entit�s (position, visibilit�) */
	void play();
};
