/*!
* \file Pacgum.h
* \author Arthur de Nanteuil, Antoine Radet
*
* \brief Les boules que Pacman doit manger
*/

#pragma once
#include <memory>
#include "FixedObject.h"
#include "GameEvent.h"

typedef enum {GUM_BASIC, GUM_CHERRY, GUM_PEPPER} PacgumType;

class Pacgum :
	public FixedObject
{
private:
	/*!< Ev�nement lanc� lorsqu'une gum est mang�e */
	std::shared_ptr<GameEvent> _eatenEvent; 

	int myGumId;	/*!< ID unique pour chaque Pacgum */

protected:
	PacgumType _type;	/*!< Type de Pacgum */
	bool _isEaten;		/*!< Etat de la Pacgum (mang�e ou non) */

	void collideCallback(bool isTouched);

public:
	Pacgum(float, float, float, VectPtrTexture, PacgumType gumType = GUM_BASIC);
	~Pacgum();

	/* Marqur la Pacgum comme �tant mang�e et met � jour l'�tat des fant�mes et de Pacman en cons�quence*/
	void setEaten();
	
	/* Renvoie vrai si la Pacgum a d�j� �t� mang�e */
	bool getEaten();

	PacgumType getPacgumType();

	/* Affiche la Pacgum (s'il n'est pas encore mang�e) */
	void draw(sf::RenderWindow &win);
};

