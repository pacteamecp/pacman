/*!
 * \file GameEventListener.h
 * \author Antoine Radet
 *
 * \brief Classe permettant aux classes d�riv�es de recevoir les informations des �v�nements gameEvent 
 */

#pragma once
class GameEventListener
{
public:
	GameEventListener();
	~GameEventListener();

	/** Fonction appel�e par un gameEvent lorsqu'il est d�clench�
	 *  Par d�faut affiche la valeur de arg sur la sortie standard
	 */
	virtual void update(int arg);
};

