/*!
* \file StatusBar.h
* \author Arthur de Nanteuil
*
* \brief Barre contenant les informations du jeu (vie restantes, points gagn�s,...)
*/

#pragma once

#include <SFML\Graphics.hpp>
#include "GameEventListener.h"
#include "Pacman.h"

class StatusBar :
	public GameEventListener
{
private:	
	sf::Texture _textLife;		/*!< Image d'une vie	*/
	sf::Sprite _spriteLife;		/*!< Sprite pour le dessin des vies restantes */
	sf::Font _scoringFont;		/*!< Police du score	*/
	sf::Text _score;			/*!< Texte du score		*/
	sf::Text _level;			/*!< Texte du niveau	*/
	int _point;					/*!< Compteur de points */
	std::shared_ptr<Pacman> _pacman;	/*!< Pointeur utile pour acc�der aux points de vie de Pacman */

public:
	StatusBar(std::shared_ptr<Pacman> pacman, std::string levelName);
	~StatusBar();

	void update(int arg);
	void draw(sf::RenderWindow &_renderWin);
};

