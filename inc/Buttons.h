/*!
* \file Buttons.h
* \author Celia Amegavie
*
* \brief Un bouton de menu
*/

#pragma once
#include "FixedObject.h"

/*bouton de taille 150*60 */
#define BUTTON_X		150.0
#define BUTTON_LARGE_X	200.0
#define BUTTON_Y		60.0

/*position des boutons*/
#define POS_BUTTON_ALL_X     170.0	// Position en x pour la plupart des boutons	
#define POS_BUTTON_LARGE_X	 145.0	// Position en x pour les boutons larges
#define POS_BUTTON_START_Y   100.0
#define POS_BUTTON_RESTART_Y 100.0
#define POS_BUTTON_RESUME_Y  210.0
#define POS_BUTTON_QUIT_Y    325.0
/*ID button:
1:START
2:RESTART
3:RESUME
4:QUIT*/

class Buttons :
	public FixedObject
{
private:
	void collideCallback(bool isTouched) {};

protected:
	int _buttonID;
	

public:
	Buttons();
	Buttons(float, float, float, VectPtrTexture, int);
	~Buttons();

	/*savoir si le bouton est color� ou pas*/
	bool is_focused = false;
	/*renvoie l'ID du bouton*/
	int getID();
	/*si on passe la souris devant le bouton(changement de texture)*/
	void isFocus();
	/*enl�ve le focus*/
	void deFocus();
};