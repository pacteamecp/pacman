/*!
* \file Map.h
* \author Antoine Radet
*
* \brief Une map contient tous les objets immobiles du jeu (murs et pacgum)
*/

#pragma once

#include <memory>
#include "SFML/Graphics.hpp"

#include "common.h"
#include "GameEventListener.h"
#include "FixedObject.h"
#include "Wall.h"
#include "Pacgum.h"

// Type de FixedObject contenu dans une case
typedef enum { MAP_NONE, MAP_WALL, MAP_GUM } fixObjType;

// La taille du tableau qui stocke les entit�s est (GRID_MAX_X + 3) x (GRID_MAX_Y + 1)
// car il y a des entit�s sur toute la largeur de la fen�tre mais pas sur toute la 
// hauteur, les dessins ne commencent qu'� la ligne 3
// Ensuite on recopie la derni�re colonne � gauche ; la premi�re colonne � droite ;
// la premi�re ligne en bas, la derni�re ligne en haut
// Ceci permet de g�rer les collisions lorsqu'on sort par un trou en haut/bas 
// ou � gauche/droite
#define NBR_MAX_X (GRID_MAX_X + 1 + 2)
#define NBR_MAX_Y (GRID_MAX_Y + 1)

class Map :
	public GameEventListener
{
public:
	Map();
	
	void add(std::shared_ptr<FixedObject> pObj, fixObjType t_obj);

	void reset();
	
	// Retourne les murs autour de la position donn�e dans le vecteur vWall
	void getWallAround(sf::Vector2f &coord, std::vector<std::shared_ptr<FixedObject>>& vWall);

	// Retourne les pacgum autour de la position donn�e dans le vecteur vGum
	void getGumAround(sf::Vector2f &coord, std::vector<std::shared_ptr<FixedObject>>& vGum);

	void draw(sf::RenderWindow &win);

	void update(int arg);

private:
	std::shared_ptr<FixedObject> myContent[NBR_MAX_X][NBR_MAX_Y];	/*!< Pointeur sur les murs ou gums de la map	*/
	fixObjType myContentType[NBR_MAX_X][NBR_MAX_Y];					/*!< Contenu de la map							*/
	int myGumCounter;												/*!< Nombre de pacgums restantes				*/

	std::shared_ptr<GameEvent> noMoreGumEvent;		/*!< Ev�nement lanc� lorsque toutes les pacgums ont �t� mang�es */
};

