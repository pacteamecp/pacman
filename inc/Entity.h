/*!
 * \file Entity.h
 * \author Arthur de Nanteuil, Franck Pagny
 *
 * \brief Une entit� repr�sente n'importe quel objet s'affichant � l'�cran
 */

#pragma once

#include "common.h"
#include <SFML/Graphics.hpp>
#include <memory>
#include <vector>

typedef enum { NONE, UP, DOWN, LEFT, RIGHT } Direction;
typedef std::vector<std::shared_ptr<sf::Texture>> VectPtrTexture;

class Entity
{
protected:
	sf::Vector2f _coord;		/*!< Coordonn�es (dans le r�p�re SFML : (0,0) en haut � gauche) */
	float _hitbox;				/*!< Taille de la hitbox (carr�e)                               */
	std::vector<sf::Sprite> _spriteVect;	/*!< Vecteurs des sprites utilis�s pour repr�senter l'objet */
	int _spriteNumber;						/*!< Indice du sprite courant                               */

	static int cptEntity;

	virtual void collideCallback(bool ) = 0;
	
public:
	Entity();
	Entity(float x0, float y0, float hb, VectPtrTexture textVect);
	~Entity();

	sf::Vector2f getCoord() const;
	void Entity::setCoord(float x, float y);

	float getHitbox() const;

	/* Change le Sprite courant � afficher pour repr�senter l'entit� */
	void set_spriteNumber(int spriteNumber);

	/* Affiche l'entit� (sous certaines conditions) */
	virtual void draw(sf::RenderWindow &win);

	/** Rend vrai si l'entit� courante va collisionner l'entit� "E" dans le cas
	 * o� l'entit� courante se d�placerait dans la direction "dir" 
	 *
	 * Si "dir" vaut NONE alors la fonction rend vrai si les deux entit�s se touche
	 * sont en collision � l'instant pr�sent
	 */
	virtual bool collide(Entity const &E, Direction dir);
};

