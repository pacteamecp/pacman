/*!
* \file Menu.h
* \author Celia Amegavie
*
* \brief Menu contenant des boutons et renvoyant le choix de l'utilisateur (ex�cution bloquante)
* Diff�rents types de Menus peuvent �tre cr��s (start, pause,...)
*/

#pragma once
#include <SFML/Graphics.hpp>
#include "PointerMouse.h"
#include "Buttons.h"


typedef enum { MENU_START, MENU_PAUSE} MenuType;
enum {BUTTON_ONE, BUTTON_START, BUTTON_BACK_TO_START, BUTTON_RESUME, BUTTON_QUIT };

#define MAX_TEX_BUTTON 20

class Menu
{
protected:
	
	int ret;							/*!< Valeur de retour des tests sur les boutons*/
	MenuType name;						/*!< Type de menu (start ou pause)*/
	PointerMouse point;					/*!< Pointeur sur la souris*/
	std::vector<Buttons> listButton;	/*!< Liste de boutons*/
	
	VectPtrTexture listTexButton;		/*!< Liste des textures des �l�ments du menu */
	sf::Sprite pacmanLogo;

public:
	Menu();
	Menu(MenuType, PointerMouse&);
	~Menu();

	/*show menu*/
	int show(sf::RenderWindow &window, sf::Event event);
};

