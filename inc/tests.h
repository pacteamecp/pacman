/*!
* \file tests.h
* \author Antoine Radet
*
* \brief Fichiers de tests
*/

#pragma once

// General C++ headers
#include <iostream>
#include <vector>
#include <memory>

// SFML headers
#include <SFML/Graphics.hpp>

// Game specific headers
#include <Scene.h>
#include <Entity.h>
#include <Pacman.h>
#include <Pacgum.h>


void test_movingObjects(sf::RenderWindow &window, sf::Event &event);
void test_scene(sf::RenderWindow &window, sf::Event &event);
void test_map(sf::RenderWindow &window, sf::Event &event);
void test_button(sf::RenderWindow &window, sf::Event &event);
