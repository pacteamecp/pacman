/*!
* \file Strategy.h
* \author Franck Pagny
*
* \brief Strategie de d�placement pour les fantomes
* La classe Strategy est abstraite, pour choisir une strat�gie particuli�re il faut 
* instancier une des diff�rente classe d�riv�e
*/

#pragma once
#include <memory>
#include "Pacman.h"
#include "Ghost.h"

class Ghost;

class Strategy
{
public:
	virtual ~Strategy() = 0;

	/* Joue la strat�gie et d�termine la prochaine direction de l'objet concern� */
	Direction play(); 
	void setPacmanPtr(std::shared_ptr<Pacman>);
	void setGhostPtr(std::weak_ptr<Ghost>);
	std::shared_ptr<Pacman> getStrategyTarget();
	std::weak_ptr<Ghost> getStrategyGhost();

protected:
	std::shared_ptr<Pacman> _pacman; /* Pointeur sur Pacman*/
	std::weak_ptr<Ghost> _ghost; /* Pointeur sur Ghost*/
	
	/* Direction d�j� tent�e */
	std::vector<bool> _impossibleDirections;

	/* Direction choisie*/
	Direction _dir;

	/* Fonction virtuelle pure d'application de la strat�gie */
	virtual Direction apply() = 0;

	/* Fonction d'�valuation des choix de direction */
	void checkDir(Direction);

	/* Fonction de choix d'une direction possible par d�fault */
	void chooseDefaultDir(Direction);
};


class Offensive 
	: public Strategy
{
public:
	Offensive();
	/* Applique une strat�gie visant � attraper MovingObject si on est proche de lui
	Sinon le fant�me se d�place al�atoirement */
	Direction apply();

protected:

	/* Cooldown de choix de la direction al�atoire */
	int cptRand;
};

class Random 
	: public Strategy
{
public:
	Random();
	/* Applique une strat�gie changeant al�atoirement de direction */
	Direction apply();
protected:


	/* Cooldown de choix de la direction al�atoire */
	int cptRand;
};

class Afraid
	: public Strategy
{
public:
	Afraid();
	/* Applique une strat�gie cherchant � fuir MovingObject*/
	Direction apply();
};

class Track
	: public Strategy
{
public:
	Track();

	/* Applique une strat�gie cherchant � fuir MovingObject*/
	Direction apply();

};
