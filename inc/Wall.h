/*!
* \file Wall.h
* \author Arthur de Nanteuil
*
* \brief Murs de la carte
*/

#pragma once
#include "FixedObject.h"
class Wall :
	public FixedObject
{
private:
	void collideCallback(bool isTouched) {};

public:
	Wall(float x0, float y0, float hb, VectPtrTexture textVect);
};

