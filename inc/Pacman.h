/*!
* \file Pacman.h
* \author Franck Pagny, Arthur de Nanteuil
*/

#pragma once

#include "MovingObject.h"
#include "GameEvent.h"
#include "GameEventListener.h"

// Le 6�me sprite (case 5) est toujours le Pacman en position ferm�e
#define SPRITE_CLOSE 5
// Temps (en nombre de frames) entre une ouverture et une fermeture de Pacman
#define T_OPEN_CLOSE 10

// Dur�e (en nombre de frames) pendant laquelle Pacman va vite apr�s avoir mang� un "pepper"
#define PACMAN_PEPPER_TIMEOUT 300

class Pacman :
	public MovingObject, public GameEventListener
{
private:
	std::shared_ptr<GameEvent> _deadEvent;	/** Ev�nement lanc� lorsque Pacman n'a plus de vie restantes */

	void collideCallback(bool isTouched) {};
	void update_spriteNumber();

public:
	Pacman(float, float, float, VectPtrTexture, Direction, int, float);

	/* Affiche Pacman (s'il est vivant) */
	void draw(sf::RenderWindow &win);

	/* D�cr�mente le temps restant de l'effet sp�cial et indique s'il est termin� */
	bool updateSpecialEffectTimer();

	/* Red�finition de la fonction kill() : lance un �v�nement avec l'ID ID_PACMAN_DEAD quand Pacman n'a plus de vie */
	void kill();

	/* Callback lors de la r�c�ption d'un �v�nement */
	void update(int arg);
};

