/*!
* \file Ghost.h
* \author Franck Pagny, Antoine Radet
*
* \brief Fantome dot� d'une strat�gie de d�placement
*/

#pragma once
#include <iostream>
#include <memory>

#include "MovingObject.h"
#include "GameEventListener.h"
#include "Strategy.h"

// Etat additionnel lorsqu'un fantome devient vuln�rable
#define SCARED 2

#define GHOST_SPRITE_SCARED 0 
#define GHOST_SPRITE_OK		1

// Dur�e (en nombre de frames) pendant laquelle les fant�mes restent vuln�rables
#define GHOST_SCARED_TIMEOUT 250

class Strategy;

class Ghost :
	public MovingObject, public GameEventListener, public std::enable_shared_from_this<Ghost>
{
protected:
	std::shared_ptr<Strategy> _currentStrategy;	/*!< Strat�gie (IA) */
	std::shared_ptr<Strategy> _mainStrategy;	/*!< Strat�gie principale (IA) */
	int _specialEffectTimer; /*!< Dur�e restante de l'effet sp�cial appliqu� sur le fant�me (en frames) */

	void collideCallback(bool isTouched) {};
	void update_spriteNumber();

public:
	Ghost();
	Ghost(float, float, float, VectPtrTexture, Direction, int, float, std::shared_ptr<Pacman>);
	~Ghost();
	
	/* Change le strat�gie employ�e par le fant�me*/
	void setCurrentStrategy(std::shared_ptr < Strategy > strategy);

	void setStrategyTarget(std::shared_ptr<Pacman> target);

	/* Change le strat�gie principale (de base) employ�e par le fant�me*/
	void setMainStrategy(std::shared_ptr < Strategy > strategy, std::shared_ptr<Pacman>);

	/* Choisi la prochaine direction dans laquelle le fant�me souhaitera se d�placer*/
	void applyStrategy(); 

	/* Modifie la dur�e de l'effet sp�cial appliqu� sur le fant�me */
	void setSpecialEffectTimer(int time);

	/* D�cr�mente le temps restant de l'effet sp�cial et indique s'il est termin� */
	bool updateSpecialEffectTimer();

	/* Red�finition de la fonction kill() : un fantome a un nombre de points de vie illimit� */
	void kill();

	/* Fonction de callback pour les �v�n�ments du jeu */
	void update(int arg);
};