/*!
* \file common.h
* \author Antoine Radet
*
* \brief Constantes utiles pour l'ensemble du jeu
*/

#pragma once

// Taille de la fen�tre (en pixels)
#define WIN_SIZE_X 500
#define WIN_SIZE_Y 500

// Taille de la grille d'affichage (un objet = une case GRID_SIZE*GRID_SIZE pixels)
#define GRID_SIZE 20

// Abscisse et ordonn�es maximales en nombre de cases de grille
#define GRID_MAX_X (WIN_SIZE_X/GRID_SIZE - 1)
#define GRID_MAX_Y (WIN_SIZE_Y/GRID_SIZE - 1)

// Abscisse minimale/maximale (en px)
#define X_MIN 0
#define X_MAX WIN_SIZE_X

// Ordonn�e  minimale/maximale (en px)
#define Y_MIN (2*GRID_SIZE)
#define Y_MAX WIN_SIZE_Y

/* IDs des �v�nements de jeu */
#define ID_GUM_EATEN		0x10
#define ID_PEPPER_EATEN		0x11
#define ID_CHERRY_EATEN		0x12
#define ID_GHOST_KILLED		0x20
#define ID_PACMAN_KILLED	0x30	// Pacman est tu� par un fantome
#define ID_PACMAN_DEAD		0x31	// Pacman n'a plus de vie
#define ID_LEVEL_FINISHED	0x40