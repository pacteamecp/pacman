/*!
* \file GameManager.h
* \author Antoine Radet
*
* \brief Gestion du d�roulement du jeu (affichage des menus, mise � jour de la sc�ne, pause,...)
*/

#pragma once
#include "GameEventListener.h"
#include "GameEvent.h"
#include "Scene.h"
#include "SFML\Graphics.hpp"

// Etat de la partie
typedef enum { GAME_NOT_STARTED, GAME_CONTINUE, GAME_LEVEL_FINISHED, GAME_OVER, GAME_RESTART } GameState;

class GameManager : 
	public GameEventListener, public std::enable_shared_from_this<GameEventListener>
{
public:
	GameManager(std::string name);
	~GameManager();

	void play();
	
	void update(int arg);

private:
	sf::RenderWindow myRenderWin;	/*!< Fen�tre pour l'affichage des objets graphiques */
	GameState myGameState;
};

